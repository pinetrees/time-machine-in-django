from django.contrib import admin
from django.core.urlresolvers import reverse
from people.models import Person, Employee, OrganizationCategory, Organization, Client, Vendor, Affiliation, SaaSProvider, SaaSSubscription, PublicProfile

class AffiliationInline(admin.TabularInline):
	model = Affiliation
	extra = 0

class SaaSSubscriptionInline(admin.TabularInline):
	model = SaaSSubscription

class PublicProfileInline(admin.TabularInline):
	model = PublicProfile

class OrganizationCategoryInline(admin.TabularInline):
	model = OrganizationCategory

class PersonAdmin(admin.ModelAdmin):
	inlines = [AffiliationInline]

class OrganizationAdmin(admin.ModelAdmin):
	inlines = [AffiliationInline]

class AffiliationAdmin(admin.ModelAdmin):
	list_display = ["organization", "person"]

class SaaSProviderAdmin(admin.ModelAdmin):
	list_display = ["organization", "number_of_employees", "date_founded", "get_categories"]
	inlines = [SaaSSubscriptionInline]

	def get_categories(self, obj):
		#This is for reference
		url = reverse('admin:people_saasprovider_changelist')
		return ", ".join([category.name for category in obj.categories.all()])

class PublicProfileAdmin(admin.ModelAdmin):
	list_display = ["organization", "ipo_date", "ipo_shares_sold", "ipo_price", "current_price"]

# Register your models here.
admin.site.register(Person, PersonAdmin)
admin.site.register(Employee)
admin.site.register(OrganizationCategory)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Client)
admin.site.register(Vendor)
admin.site.register(Affiliation, AffiliationAdmin)
admin.site.register(SaaSProvider, SaaSProviderAdmin)
admin.site.register(PublicProfile, PublicProfileAdmin)
