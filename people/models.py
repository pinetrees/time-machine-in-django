from django.db import models
from django.utils import timezone
from model_utils.models import TimeStampedModel
from core.models import NamedModel

# Create your models here.
class Person(TimeStampedModel):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)

	def __str__(self):
		return " ".join([self.first_name, self.last_name])

	class Meta:
		verbose_name_plural = "People"

class Employee(Person):

	@classmethod
	def from_person(self, person):
		employee = self(person_ptr=person)
		employee.__dict__.update(person.__dict__)
		employee.save()
		return employee
	
class OrganizationCategory(NamedModel):

	class Meta:
		verbose_name = "Category"
		verbose_name_plural = "Categories"

class Organization(TimeStampedModel):
	name = models.CharField(max_length=200)
	people = models.ManyToManyField(Person, through="Affiliation")

	def __str__(self):
		return self.name

class Client(TimeStampedModel):
	organization = models.ForeignKey(Organization)
	relationship_status = models.DecimalField(max_digits=3, decimal_places=2, null=True, blank=True)

	def __str__(self):
		return self.organization.__str__()

	@classmethod
	def create_as_person(self, first_name, last_name):
		person = Person.objects.get_or_create(first_name=first_name, last_name=last_name)[0]
		organization = Organization.objects.get_or_create(name=" ".join([first_name, last_name]))[0]
		affiliation = Affiliation.objects.get_or_create(organization=organization, person=person, context="Self")
		client = self.objects.get_or_create(organization=organization)
		return client

class Vendor(models.Model):
	organization = models.ForeignKey(Organization)

	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.organization.__str__()

class Affiliation(TimeStampedModel):
	organization = models.ForeignKey(Organization)
	person = models.ForeignKey(Person)
	context = models.CharField(max_length=200, null=True, blank=True)
	
class SaaSProvider(Vendor):
	description = models.TextField()
	number_of_employees = models.IntegerField(default=1)
	date_founded = models.DateField(null=True, blank=True)
	categories = models.ManyToManyField(OrganizationCategory)

	class Meta:
		verbose_name = "SaaS Provider"
		verbose_name_plural = "SaaS Providers"

class SaaSSubscription(NamedModel):
	provider = models.ForeignKey(SaaSProvider)
	cost = models.DecimalField(max_digits=6, decimal_places=2)
	PERIODS = (
		(1, "Monthly"),
		(2, "Yearly"),
	)
	allowable_users = models.IntegerField(default=1)
	frequency = models.IntegerField(choices=PERIODS, default=1)

	class Meta:
		verbose_name = "Subscription Option"
		verbose_name_plural = "Subscription Options"

class PublicProfile(models.Model):
	organization = models.ForeignKey(Organization)
	ipo_date = models.DateField(null=True)
	ipo_shares_sold = models.IntegerField(null=True)
	ipo_price = models.DecimalField(max_digits=6, decimal_places=2, null=True)
	current_price = models.DecimalField(max_digits=6, decimal_places=2, null=True)
	created_at = models.DateTimeField(auto_now_add=True, default=timezone.now)
	modified_at = models.DateTimeField(auto_now=True, default=timezone.now)
	pass

