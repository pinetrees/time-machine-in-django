# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0025_auto_20150104_0245'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrganizationCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=200)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='saasprovider',
            name='categories',
            field=models.ManyToManyField(to='people.OrganizationCategory'),
            preserve_default=True,
        ),
    ]
