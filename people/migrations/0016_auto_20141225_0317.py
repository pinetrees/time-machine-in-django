# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0015_auto_20141225_0316'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='affiliation',
            name='created',
        ),
        migrations.RemoveField(
            model_name='affiliation',
            name='modified',
        ),
    ]
