# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0019_auto_20141225_0318'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='relationship_status',
            field=models.DecimalField(null=True, max_digits=3, decimal_places=2, blank=True),
            preserve_default=True,
        ),
    ]
