# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0022_saasprovider'),
    ]

    operations = [
        migrations.CreateModel(
            name='SaaSSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=200)),
                ('cost', models.DecimalField(max_digits=6, decimal_places=2)),
                ('frequency', models.IntegerField(default=1, choices=[(1, b'Monthly'), (2, b'Yearly')])),
                ('provider', models.ForeignKey(to='people.SaaSProvider')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='saasprovider',
            options={'verbose_name_plural': 'SaaS Providers'},
        ),
        migrations.AddField(
            model_name='saasprovider',
            name='number_of_employees',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
