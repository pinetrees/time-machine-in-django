# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0021_auto_20150103_2008'),
    ]

    operations = [
        migrations.CreateModel(
            name='SaaSProvider',
            fields=[
                ('vendor_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='people.Vendor')),
                ('description', models.TextField()),
            ],
            options={
            },
            bases=('people.vendor',),
        ),
    ]
