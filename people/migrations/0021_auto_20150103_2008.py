# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0020_auto_20141225_2042'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('person_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='people.Person')),
            ],
            options={
                'abstract': False,
            },
            bases=('people.person',),
        ),
        migrations.AddField(
            model_name='organization',
            name='people',
            field=models.ManyToManyField(to='people.Person', through='people.Affiliation'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='affiliation',
            name='context',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
