# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0028_auto_20150104_0327'),
    ]

    operations = [
        migrations.RenameField(
            model_name='publicprofile',
            old_name='ipo_percent_sold',
            new_name='ipo_shares_sold',
        ),
    ]
