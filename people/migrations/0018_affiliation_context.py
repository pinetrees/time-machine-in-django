# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0017_remove_affiliation_context'),
    ]

    operations = [
        migrations.AddField(
            model_name='affiliation',
            name='context',
            field=models.CharField(max_length=200, null=True),
            preserve_default=True,
        ),
    ]
