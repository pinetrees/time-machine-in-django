# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0011_vendor'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vendor',
            name='organization',
        ),
        migrations.DeleteModel(
            name='Vendor',
        ),
    ]
