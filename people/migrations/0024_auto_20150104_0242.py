# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0023_auto_20150104_0235'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='saasprovider',
            options={'verbose_name': 'SaaS Provider', 'verbose_name_plural': 'SaaS Providers'},
        ),
        migrations.AlterModelOptions(
            name='saassubscription',
            options={'verbose_name': 'Subscription Option', 'verbose_name_plural': 'Subscription Options'},
        ),
        migrations.AddField(
            model_name='saassubscription',
            name='allowable_users',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
