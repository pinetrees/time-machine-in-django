# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0014_affiliation'),
    ]

    operations = [
        migrations.AddField(
            model_name='affiliation',
            name='context',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='affiliation',
            name='organization',
            field=models.ForeignKey(default=1, to='people.Organization'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='affiliation',
            name='person',
            field=models.ForeignKey(default=1, to='people.Person'),
            preserve_default=False,
        ),
    ]
