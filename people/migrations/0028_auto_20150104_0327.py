# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0027_auto_20150104_0300'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='organizationcategory',
            options={'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
        migrations.AddField(
            model_name='publicprofile',
            name='organization',
            field=models.ForeignKey(default=1, to='people.Organization'),
            preserve_default=False,
        ),
    ]
