from django.db import models

# Create your models here.
class HarvestUser(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	is_active = models.BooleanField(default=True)
	updated_at = models.DateTimeField(auto_now=True)
	default_hourly_rate = models.DecimalField(max_digits=6, decimal_places=2, default=0.0)
	email = models.CharField(max_length=200)
	is_admin = models.BooleanField(default=False)
	cost_rate = models.DecimalField(max_digits=6, decimal_places=2, default=0.0)
	identity_account_id = models.IntegerField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return " ".join([self.first_name, self.last_name])

class HarvestClient(models.Model):
	name = models.CharField(max_length=200)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	details = models.TextField(null=True, default='')
	active = models.BooleanField(default=True)

	def __str__(self):
		return self.name

class HarvestProject(models.Model):
	name = models.CharField(max_length=200)
	code = models.CharField(max_length=5)
	client = models.ForeignKey(HarvestClient, null=True)
	show_budget_to_all = models.BooleanField(default=False)
	updated_at = models.DateTimeField(auto_now=True)
	cost_budget = models.DecimalField(max_digits=7, decimal_places=2, null=True)
	budget = models.DecimalField(max_digits=7, decimal_places=2, null=True)
	budget_by = models.CharField(max_length=200)
	estimate = models.DecimalField(max_digits=7, decimal_places=2, null=True)
	hourly_rate = models.DecimalField(max_digits=6, decimal_places=2, null=True)
	billable = models.BooleanField(default=True)
	estimate_by = models.CharField(max_length=200)
	active = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	cost_budget_include_expenses = models.BooleanField(default=False)
	notes = models.TextField(null=True)

	def __str__(self):
		return self.name

class HarvestTask(models.Model):
	name = models.CharField(max_length=200)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_default = models.BooleanField(default=True)
	default_hourly_rate = models.DecimalField(max_digits=6, decimal_places=2, default=0.0)

	def __str__(self):
		return self.name

class HarvestTaskAssignment(models.Model):
	project = models.ForeignKey(HarvestProject, null=True)
	task = models.ForeignKey(HarvestTask, null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	budget = models.DecimalField(max_digits=7, decimal_places=2, null=True)
	billable = models.BooleanField(default=True)
	estimate = models.DecimalField(max_digits=7, decimal_places=2, null=True)
	hourly_rate = models.DecimalField(max_digits=6, decimal_places=2, default=0.0)
	deactivated = models.BooleanField(default=False)


class HarvestEntry(models.Model):
	user = models.ForeignKey(HarvestUser, null=True)
	task = models.ForeignKey(HarvestTask, null=True)
	project = models.ForeignKey(HarvestProject, null=True)
	spent_at = models.DateTimeField(null=True)
	is_billed = models.BooleanField(default=False)
	notes = models.TextField(null=True)
	timer_started_at = models.DateTimeField(null=True)
	hours = models.DecimalField(max_digits=4, decimal_places=2, default=0.0)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	adjustment_record = models.BooleanField(default=False)
	is_closed = models.BooleanField(default=False)

	class Meta:
		verbose_name_plural = "Harvest entries"

