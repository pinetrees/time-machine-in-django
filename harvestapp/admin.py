from django.contrib import admin
from harvestapp.models import HarvestUser, HarvestClient, HarvestProject, HarvestTask, HarvestTaskAssignment, HarvestEntry

class HarvestUserAdmin(admin.ModelAdmin):
	pass

class HarvestEntryAdmin(admin.ModelAdmin):
	list_display = ["user", "project", "task", "hours"]
	list_filter = ["user", "project"]

# Register your models here.
admin.site.register(HarvestUser)
admin.site.register(HarvestClient)
admin.site.register(HarvestProject)
admin.site.register(HarvestTask)
admin.site.register(HarvestTaskAssignment)
admin.site.register(HarvestEntry, HarvestEntryAdmin)
