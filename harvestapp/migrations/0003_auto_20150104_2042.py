# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0002_user_harvest_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='HarvestProject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('harvest_id', models.IntegerField(null=True)),
                ('name', models.CharField(max_length=200)),
                ('code', models.CharField(max_length=5)),
                ('show_budget_to_all', models.BooleanField(default=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('cost_budget', models.DecimalField(null=True, max_digits=7, decimal_places=2)),
                ('budget', models.DecimalField(null=True, max_digits=7, decimal_places=2)),
                ('budget_by', models.CharField(max_length=200)),
                ('estimate', models.DecimalField(null=True, max_digits=7, decimal_places=2)),
                ('hourly_rate', models.DecimalField(null=True, max_digits=6, decimal_places=2)),
                ('billable', models.BooleanField(default=True)),
                ('estimate_by', models.CharField(max_length=200)),
                ('client_id', models.IntegerField(null=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('cost_budget_include_expenses', models.BooleanField(default=False)),
                ('notes', models.TextField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HarvestUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('harvest_id', models.IntegerField(null=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('is_active', models.BooleanField(default=True)),
                ('default_hourly_rate', models.DecimalField(default=0.0, max_digits=6, decimal_places=2)),
                ('email', models.CharField(max_length=200)),
                ('is_admin', models.BooleanField(default=False)),
                ('cost_rate', models.DecimalField(default=0.0, max_digits=6, decimal_places=2)),
                ('identity_account_id', models.IntegerField(null=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='User',
        ),
    ]
