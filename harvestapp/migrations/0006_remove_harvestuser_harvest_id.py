# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0005_auto_20150104_2103'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='harvestuser',
            name='harvest_id',
        ),
    ]
