# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0008_harvesttask'),
    ]

    operations = [
        migrations.CreateModel(
            name='HarvestEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.IntegerField(null=True)),
                ('task_id', models.IntegerField(null=True)),
                ('project_id', models.IntegerField(null=True)),
                ('spent_at', models.DateTimeField(null=True)),
                ('is_billed', models.BooleanField(default=False)),
                ('notes', models.TextField(null=True)),
                ('timer_started_at', models.DateTimeField(null=True)),
                ('hours', models.DecimalField(default=0.0, max_digits=4, decimal_places=2)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('adjustment_record', models.BooleanField(default=False)),
                ('is_closed', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
