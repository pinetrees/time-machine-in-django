# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0011_auto_20150104_2130'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='harvestentry',
            name='task_id',
        ),
        migrations.AddField(
            model_name='harvestentry',
            name='task',
            field=models.ForeignKey(to='harvestapp.HarvestTask', null=True),
            preserve_default=True,
        ),
    ]
