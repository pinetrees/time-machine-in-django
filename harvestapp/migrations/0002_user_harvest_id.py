# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='harvest_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
