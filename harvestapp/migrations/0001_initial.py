# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('is_active', models.BooleanField(default=True)),
                ('default_hourly_rate', models.DecimalField(default=0.0, max_digits=6, decimal_places=2)),
                ('email', models.CharField(max_length=200)),
                ('is_admin', models.BooleanField(default=False)),
                ('cost_rate', models.DecimalField(default=0.0, max_digits=6, decimal_places=2)),
                ('identity_account_id', models.IntegerField(null=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
