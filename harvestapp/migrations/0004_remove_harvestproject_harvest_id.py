# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0003_auto_20150104_2042'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='harvestproject',
            name='harvest_id',
        ),
    ]
