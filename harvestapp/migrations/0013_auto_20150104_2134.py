# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0012_auto_20150104_2131'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='harvestentry',
            name='project_id',
        ),
        migrations.RemoveField(
            model_name='harvestproject',
            name='client_id',
        ),
        migrations.AddField(
            model_name='harvestentry',
            name='project',
            field=models.ForeignKey(to='harvestapp.HarvestProject', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='harvestproject',
            name='client',
            field=models.ForeignKey(to='harvestapp.HarvestClient', null=True),
            preserve_default=True,
        ),
    ]
