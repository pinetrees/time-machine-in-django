# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0010_auto_20150104_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='harvestentry',
            name='user',
            field=models.ForeignKey(to='harvestapp.HarvestUser', null=True),
            preserve_default=True,
        ),
    ]
