# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0013_auto_20150104_2134'),
    ]

    operations = [
        migrations.CreateModel(
            name='HarvestTaskAssignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('budget', models.DecimalField(null=True, max_digits=7, decimal_places=2)),
                ('billable', models.BooleanField(default=True)),
                ('estimate', models.DecimalField(null=True, max_digits=7, decimal_places=2)),
                ('default_hourly_rate', models.DecimalField(default=0.0, max_digits=6, decimal_places=2)),
                ('deactivated', models.BooleanField(default=False)),
                ('project', models.ForeignKey(to='harvestapp.HarvestProject', null=True)),
                ('task', models.ForeignKey(to='harvestapp.HarvestTask', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
