# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0014_harvesttaskassignment'),
    ]

    operations = [
        migrations.RenameField(
            model_name='harvesttaskassignment',
            old_name='default_hourly_rate',
            new_name='hourly_rate',
        ),
    ]
