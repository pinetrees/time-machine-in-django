# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0006_remove_harvestuser_harvest_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='harvestclient',
            name='active',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='harvestclient',
            name='details',
            field=models.TextField(default=b'', null=True),
            preserve_default=True,
        ),
    ]
