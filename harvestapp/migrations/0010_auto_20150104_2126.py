# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harvestapp', '0009_harvestentry'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='harvestentry',
            options={'verbose_name_plural': 'Harvest entries'},
        ),
        migrations.RemoveField(
            model_name='harvestentry',
            name='user_id',
        ),
        migrations.AddField(
            model_name='harvestentry',
            name='user',
            field=models.ForeignKey(default=1, to='harvestapp.HarvestUser'),
            preserve_default=False,
        ),
    ]
