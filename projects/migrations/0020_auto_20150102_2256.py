# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0019_task_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='hours_worked',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='task',
            name='minutes_worked',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='task',
            name='priority',
            field=models.IntegerField(default=2, choices=[(1, b'High'), (2, b'Medium'), (3, b'Low')]),
            preserve_default=True,
        ),
    ]
