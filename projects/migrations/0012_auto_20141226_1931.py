# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0011_auto_20141226_1931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='priority',
            field=models.IntegerField(default=2, choices=[(1, b'High'), (2, b'Medium'), (3, b'Low')]),
            preserve_default=True,
        ),
    ]
