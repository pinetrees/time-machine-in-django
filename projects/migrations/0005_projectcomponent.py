# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0004_auto_20141223_1633'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectComponent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('value', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('billed', models.BooleanField(default=False)),
                ('paid', models.BooleanField(default=False)),
                ('project', models.ForeignKey(to='projects.Project')),
                ('status', models.ForeignKey(default=1, to='projects.ProjectStatus')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
