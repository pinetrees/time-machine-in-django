# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0015_project_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='due',
            field=models.DateField(default=datetime.date.today),
            preserve_default=True,
        ),
    ]
