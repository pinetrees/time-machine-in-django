# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0013_vendor'),
        ('projects', '0006_remove_project_value'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='client',
            field=models.ForeignKey(default=1, to='people.Client'),
            preserve_default=False,
        ),
    ]
