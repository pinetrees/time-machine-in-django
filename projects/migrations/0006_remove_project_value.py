# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0005_projectcomponent'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='value',
        ),
    ]
