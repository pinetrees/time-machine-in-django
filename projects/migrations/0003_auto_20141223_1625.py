# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0002_project_project_code'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='project_code',
            new_name='code',
        ),
        migrations.AddField(
            model_name='project',
            name='value',
            field=models.DecimalField(default=0, max_digits=10, decimal_places=2),
            preserve_default=True,
        ),
    ]
