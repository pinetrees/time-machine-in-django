#from django.db import models
from core import models
from people.models import Client
from django.contrib.auth.models import User
from django.utils import timezone
import datetime

# Create your models here.
class ProjectStatus(models.TimeMachineModel):
	name = models.CharField(max_length=200)

class Project(models.TimeMachineModel):
	name = models.CharField(max_length=200)
	client = models.ForeignKey(Client, null=True)
	code = models.CharField(max_length=5, null=True, blank=True)
	#value = models.DecimalField(max_digits=10, decimal_places=2, default=0)
	notes = models.TextField(null=True, blank=True)
	due = models.DateField(default=datetime.date.today)
	manager = models.ForeignKey(User, null=True, blank=True)
	status = models.ForeignKey(ProjectStatus, default=1)
	
	PRIORITY_CHOICES = (
		(1, 'High'),
		(2, 'Medium'), 
		(3, 'Low'), 	
	)
	priority = models.IntegerField(choices=PRIORITY_CHOICES, default=2)

	def value(self):
		value = self.projectcomponent_set.aggregate(models.Sum('value'))['value__sum']
		if value == None:
			return 0.00
		else:
			return value

	def aggregate_status(self):
		if self.projectcomponent_set.filter(status__name='Active').count() > 0:
			return ProjectStatus.objects.filter(name='Active').first()
		elif self.projectcomponent_set.exclude(status__name='Complete').count() == 0:
			return ProjectStatus.objects.filter(name='Complete').first()
		else:
			return ProjectStatus.objects.filter(name='Pending').first()

	def unbilled(self):
		value = self.projectcomponent_set.filter(billed=0).aggregate(models.Sum('value'))['value__sum']
		if value == None:
			return 0.00
		else:
			return value

	def save(self, *args, **kwargs):
		self.status = self.aggregate_status()
		super(Project, self).save(*args, **kwargs)

class ProjectComponent(models.TimeMachineModel):
	name = models.CharField(max_length=200)
	project = models.ForeignKey(Project)
	value = models.DecimalField(max_digits=10, decimal_places=2, default=0)
	status = models.ForeignKey(ProjectStatus, default=1)
	billed = models.BooleanField(default=False)
	paid = models.BooleanField(default=False)

class WorkLog(models.Model):
	date = models.DateField(default=datetime.date.today)
	notes = models.TextField(null=True)

class Task(models.Model):
	person = models.ForeignKey(User, null=True, blank=True)
	project = models.ForeignKey(Project, null=True, blank=True)
	date = models.DateField(default=datetime.date.today)
	description = models.TextField(null=True)
	PRIORITY_CHOICES = (
		(1, 'High'),
		(2, 'Medium'), 
		(3, 'Low'), 	
	)
	priority = models.IntegerField(choices=PRIORITY_CHOICES, default=2)
	complete = models.BooleanField(default=False)
	hours_worked = models.IntegerField(default=0)
	minutes_worked = models.IntegerField(default=0)

	def __str__(self):
		return self.description
	

def update_project_status_on_project_component_save(instance, created, raw, **kwargs):
	aggregated_status = instance.project.aggregate_status()
	instance.project.status = aggregated_status
	instance.project.save()

models.signals.post_save.connect(update_project_status_on_project_component_save, sender=ProjectComponent, dispatch_uid="update_project_status_on_project_component_save")
