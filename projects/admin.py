from django.contrib import admin
from projects.models import Project, ProjectComponent, WorkLog, Task

class ProjectComponentInline(admin.TabularInline):
	model = ProjectComponent

class TaskInline(admin.TabularInline):
	model = Task

class ProjectAdmin(admin.ModelAdmin):
	list_display = ["__str__", "client", "manager", "value", "unbilled", "status", "priority"]
	list_editable = ["manager", "priority"]
	list_filter = ["status", "manager", "priority"]
	inlines = [ProjectComponentInline, TaskInline]

class ProjectComponentAdmin(admin.ModelAdmin):
	list_filter = ["project"]

class TaskAdmin(admin.ModelAdmin):
	list_display = ["project", "description", "date"]
	list_filter = ["person", "project"]

# Register your models here.
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectComponent, ProjectComponentAdmin)
admin.site.register(WorkLog)
admin.site.register(Task, TaskAdmin)
