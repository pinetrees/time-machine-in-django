from django.db.models import *
import datetime

class TimeMachineModel(Model):
	
	def __str__(self):
		return self.name

	class Meta:
		abstract = True

CoreModel = TimeMachineModel

class NamedModel(Model):
	name = CharField(max_length=200, default='')
	
	def __str__(self):
		return self.name

	class Meta:
		abstract = True

class DateModel(Model):
	date = DateField(default=datetime.date.today)

	class Meta:
		abstract = True
