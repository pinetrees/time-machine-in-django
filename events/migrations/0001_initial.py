# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import timemachine._time_


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0022_auto_20150103_2034'),
        ('people', '0029_auto_20150104_0331'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.DateTimeField(default=django.utils.timezone.now)),
                ('end_time', models.DateTimeField(default=timemachine._time_.thirty_minutes_from_now)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Meeting',
            fields=[
                ('event_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='events.Event')),
            ],
            options={
            },
            bases=('events.event',),
        ),
        migrations.AddField(
            model_name='event',
            name='people',
            field=models.ManyToManyField(to='people.Person', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='project',
            field=models.ForeignKey(blank=True, to='projects.Project', null=True),
            preserve_default=True,
        ),
    ]
