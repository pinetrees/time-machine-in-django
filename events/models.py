from django.db import models
from django.utils import timezone
import datetime
from timemachine import _time_
from people.models import Person
from projects.models import Project

# Create your models here.
class Event(models.Model):
	start_time = models.DateTimeField(default=timezone.now)
	end_time = models.DateTimeField(default=_time_.thirty_minutes_from_now)
	people = models.ManyToManyField(Person, blank=True)
	project = models.ForeignKey(Project, null=True, blank=True)

class Meeting(Event):
	pass
