from django.contrib import admin
from events.models import Event, Meeting

class MeetingAdmin(admin.ModelAdmin):
	list_display = ["start_time", "end_time", "project"]

# Register your models here.
admin.site.register(Event)
admin.site.register(Meeting, MeetingAdmin)
