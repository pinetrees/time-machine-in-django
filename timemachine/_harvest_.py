import sys
from harvest import Harvest, HarvestError
from harvestapp.models import HarvestUser, HarvestClient, HarvestProject, HarvestTask, HarvestTaskAssignment, HarvestEntry
from datetime import datetime, timedelta
import time

URI = 'https://tier27.harvestapp.com'
EMAIL = 'joshua@tier27.com'
PASS = 'piano1!9(0)'

h = Harvest(URI,EMAIL,PASS)

def import_user_by_name(first_name, last_name):
	user = h.find_user(first_name, last_name)
	if user is None:
		return False
	return import_user(user)

def import_user(user):
	tm_user = HarvestUser.objects.get_or_create(id=user.id)[0]
	#keys = ["first_name", "last_name", "is_active", "updated_at", "default_hourly_rate", "email", "is_admin", "cost_rate", "identity_account_id", "updated_at"]
	keys = [str(field.name) for field in HarvestUser._meta.fields]
	for key in keys:
		try:
			value = getattr(user, key)
			setattr(tm_user, key, value)
		except:
			print "There was some sort of error..."
	tm_user.save()
	return tm_user	

def import_users():
	for user in h.users():
		import_user(user)


def import_client(client):
	tm_client = HarvestClient.objects.get_or_create(id=client.id)[0]
	keys = [str(field.name) for field in HarvestClient._meta.fields]
	for key in keys:
		try:
			value = getattr(client, key)
			if value is not '':
				setattr(tm_client, key, value)
		except:
			print "There was some sort of error..."
	tm_client.save()
	return tm_client

def import_clients():
	for client in h.clients():
		import_client(client)

def import_project(project):
	tm_project = HarvestProject.objects.get_or_create(id=project.id)[0]
	keys = [str(field.name) for field in HarvestProject._meta.fields]
	keys = ['client_id' if key=='client' else key for key in keys]
	for key in keys:
		try:
			value = getattr(project, key)
			if value is not '':
				setattr(tm_project, key, value)
		except:
			print "There was some sort of error..."
	tm_project.save()
	return tm_project

def import_projects():
	for project in h.projects():
		import_project(project)

def import_task(task):
	tm_task = HarvestTask.objects.get_or_create(id=task.id)[0]
	keys = [str(field.name) for field in HarvestTask._meta.fields]
	for key in keys:
		try:
			value = getattr(task, key)
			if value is not '':
				setattr(tm_task, key, value)
		except:
			print "There was some sort of error..."
	tm_task.save()
	return tm_task

def import_tasks():
	for task in h.tasks():
		import_task(task)

def import_task_assignment(task_assignment):
	tm_task_assignment = HarvestTaskAssignment.objects.get_or_create(id=task_assignment.id)[0]
	keys = [str(field.name) for field in HarvestTaskAssignment._meta.fields]
	keys = ['task_id' if key=='task' else key for key in keys]
	keys = ['project_id' if key=='project' else key for key in keys]
	for key in keys:
		try:
			value = getattr(task_assignment, key)
			if value is not '':
				setattr(tm_task_assignment, key, value)
		except:
			print "There was an error saving key " + key
	tm_task_assignment.save()
	return tm_task_assignment

def import_task_assignments_for_project(project):
	for task_assignment in project.task_assignments:
		import_task_assignment(task_assignment)

def import_task_assignments():
	for project in h.projects():
		import_task_assignments_for_project(project)

def import_entry(entry):
	tm_entry = HarvestEntry.objects.get_or_create(id=entry.id)[0]
	keys = [str(field.name) for field in HarvestEntry._meta.fields]
	keys = ['user_id' if key=='user' else key for key in keys]
	keys = ['task_id' if key=='task' else key for key in keys]
	keys = ['project_id' if key=='project' else key for key in keys]
	for key in keys:
		try:
			value = getattr(entry, key)
			if value is not '':
				setattr(tm_entry, key, value)
		except:
			print "There was some sort of error..."
	tm_entry.save()
	return tm_entry

def import_entries():
	for user in h.users():
		for entry in user_entries(user):
			import_entry(entry)

def first_user():
	return list(h.users())[0]

def first_client():
	return list(h.clients())[0]

def first_project():
	return list(h.projects())[0]

def first_task():
	return list(h.tasks())[0]

def first_task_assignment():
	return list(first_project().task_assignments)[0]

def list_keys(obj):
	for key in obj.__dict__:
		print key

def user_entries(user):
	return user.entries(datetime.now() - timedelta(days=365), datetime.now())

def first_user_entry(user):
	return list(user_entries(user))[0]

def import_first_entry():
	return import_entry(first_user_entry(first_user()))
