-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: timemachine_in_django
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts_hostingaccount`
--

DROP TABLE IF EXISTS `accounts_hostingaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_hostingaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `domain` varchar(128) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `approximate_mb` int(11) NOT NULL,
  `activity_ranking` decimal(3,2) NOT NULL,
  `ip` varchar(32) NOT NULL,
  `email_accounts` int(11) NOT NULL,
  `current_server` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `site_compromised` tinyint(1) NOT NULL,
  `site_down` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_hostingaccount_ef42673f` (`subscription_id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_hostingaccount`
--

LOCK TABLES `accounts_hostingaccount` WRITE;
/*!40000 ALTER TABLE `accounts_hostingaccount` DISABLE KEYS */;
INSERT INTO `accounts_hostingaccount` VALUES (1,'abundant','abundantsociety.com',1,325,0.00,'207.210.200.101',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,0),(2,'alertinf','alert-system.info',2,1,0.00,'143.95.95.241',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,1),(3,'wwwalert','alert-system.net',2,1,0.00,'143.95.95.241',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,1),(4,'wwwalleg','alleghenysecurity.com',2,67,0.00,'143.95.95.241',6,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(5,'boldnewe','boldneweconomy.com',1,121,0.65,'207.210.200.101',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,0),(6,'wwwcalvertpearso','calvertpearson.com',3,29,0.59,'207.210.200.101',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,0),(7,'rlmohl2','casademetta.org ',1,113,0.32,'143.95.95.241',2,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(8,'mia','chain2prosper.com',1,2837,0.04,'207.210.200.101',1,'Demeter','2014-12-06 21:51:43','2014-12-20 22:36:22',0,0),(9,'chewchest','chewchest.com ',4,26,0.49,'207.210.200.101',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,0),(10,'consumer','consumerswin.com',1,1462,0.53,'143.95.95.241',3,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(11,'wwwcourt','courtierwines.com ',5,7,0.17,'143.95.95.241',1,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(12,'devtier2','dev.tier27.com ',6,345,0.01,'143.95.95.241',1,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(13,'environmentalser','environmentalservicesmarket.com',8,44,0.52,'143.95.95.241',0,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(14,'eventsca','eventscalendarinfo.com',9,7922,0.73,'143.95.95.241',0,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(15,'wwwfidir','fidirentals.com ',11,5030,0.01,'143.95.95.241',0,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(16,'booklovers','forbookloversonly.com',1,21,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,0),(17,'getreadytoprofit','getreadytoprofit.com',1,47,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:43','2014-12-22 04:08:09',0,0),(18,'ggarnes','glenngarnes.com',1,1529,0.43,'143.95.95.241',1,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(19,'instantr','instantresource.com',1,1,0.05,'143.95.95.241',0,'Demeter','2014-12-06 21:51:43','2014-12-21 03:29:54',0,0),(20,'joshuabk','joshua-kornreich.com',6,3293,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',0,0),(21,'g0tju1ce','juicingwithmia.com',1,123,0.04,'207.210.200.101',1,'Demeter','2014-12-06 21:51:44','2014-12-20 22:32:14',0,0),(22,'leadbook','leadbooksystem.com',1,2445,0.15,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-20 00:28:36',0,0),(23,'wwwlintcondition','lintcondition.com',6,156,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-20 00:28:30',0,0),(24,'wwwmanha','manhattanresidentialgroup.com',11,2540,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-20 00:28:26',0,0),(25,'maryland','marylanddentistsilverspring.com',12,364,0.81,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',0,0),(26,'wwwmissionbowlin','missionbowlingclub.com',13,930,0.97,'143.95.95.241',16,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(27,'melissa','mmctv.org',1,27,0.11,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-26 18:50:13',0,0),(28,'networki','networkingadvocate.com ',10,2518,0.49,'143.95.95.241',5,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(29,'nittanysportshud','nittanysportshuddle.com',14,97,0.51,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-20 00:27:45',0,0),(30,'photosby','photosbymohl.com',1,90,0.14,'207.210.200.101',3,'Demeter','2014-12-06 21:51:44','2014-12-20 22:37:47',0,0),(31,'wwwplaza','plazawarren.com',15,2,0.05,'143.95.95.241',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',0,1),(32,'alipasku','rfptrainer.com',1,148,0.51,'143.95.95.241',4,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(33,'ronmetta','rlmohl.com',1,1562,0.25,'143.95.95.241',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',1,0),(34,'wwwryand','ryandouvlos.com',6,7,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:09:55',0,0),(35,'smallbiz','sbceo.com ',1,11206,0.63,'143.95.95.241',1,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(36,'wwwstrat','strategicfinancialservices.net ',16,14,0.71,'143.95.95.241',2,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(37,'techimag','techimagemarketing.com ',9,290,0.81,'143.95.95.241',3,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(38,'wwwtheko','thekornreich.com',17,1,0.01,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',0,0),(39,'tiercom','tier27.com ',6,12303,0.91,'143.95.95.241',26,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(40,'wwwtier2','tier27.org ',6,64,0.05,'143.95.95.241',6,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(41,'wwwunite','unitederie.com',18,33,0.87,'143.95.95.241',1,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(42,'wwwutemu','utemusication.com',19,249,0.31,'143.95.95.241',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',0,1),(43,'villagec','villageconnector.com ',1,67480,0.63,'143.95.95.241',11,'Demeter','2014-12-06 21:51:44','2014-12-26 18:58:45',0,0),(44,'wanderdo','wanderdogadventures.com ',4,133,0.25,'207.210.200.101',0,'Demeter','2014-12-06 21:51:44','2014-12-22 04:08:09',0,0),(45,'writerss','writerssilo.com ',1,221,0.15,'143.95.95.241',1,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(46,'rblevins','yorktownesports.com',20,11051,0.89,'143.95.95.241',14,'Demeter','2014-12-06 21:51:44','2014-12-21 03:29:54',0,0),(47,'consilco','consiliumcounsel.com',7,48,0.25,'198.20.94.114',1,'Easy Business','2014-12-06 21:51:44','2014-12-25 22:54:32',0,0),(48,'danamebc','dabcla.com',7,23,0.25,'198.20.94.114',2,'Easy Business','2014-12-06 21:51:44','2014-12-25 22:54:32',0,0),(49,'kpiincco','kindredproactiveinitiative.com',7,50,0.25,'198.20.94.114',3,'Easy Business','2014-12-06 21:51:44','2014-12-25 22:54:32',0,0),(50,'mastermy','mastermylifenow.com',7,91,0.25,'198.20.94.114',4,'Easy Business','2014-12-06 21:51:45','2014-12-25 22:54:32',0,0),(51,'missn365','missionary365.com',7,85,0.25,'50.63.202.89',2,'Easy Business','2014-12-06 21:51:45','2014-12-25 22:54:32',0,0),(52,'myezbusi','myezbusiness.com',7,63,0.25,'198.20.94.114',4,'Easy Business','2014-12-06 21:51:45','2014-12-25 22:54:32',0,0),(53,'oilinyou','oilinyourlamp.com',7,63,0.25,'198.20.94.114',0,'Easy Business','2014-12-06 21:51:45','2014-12-21 03:29:54',0,0),(54,'shekgard','shekinahgarden.com',7,71,0.25,'198.20.94.114',3,'Easy Business','2014-12-06 21:51:45','2014-12-25 22:52:35',0,0),(55,'Govcon','govcononline.com',1,50,0.10,'143.95.95.241',0,'Demeter','2014-12-30 01:03:43','2014-12-30 01:03:43',0,0);
/*!40000 ALTER TABLE `accounts_hostingaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_subscription`
--

DROP TABLE IF EXISTS `accounts_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `revenue` decimal(5,2) NOT NULL,
  `periods_per_year` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_subscription_2bfe9d72` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_subscription`
--

LOCK TABLES `accounts_subscription` WRITE;
/*!40000 ALTER TABLE `accounts_subscription` DISABLE KEYS */;
INSERT INTO `accounts_subscription` VALUES (1,7,99.99,12,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(2,13,149.99,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(3,14,49.99,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(4,17,99.99,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(5,18,0.00,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(6,9,0.00,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(7,24,29.99,12,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(8,19,99.99,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(9,8,149.99,1,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(10,26,0.00,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(11,20,99.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(12,25,49.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(13,5,99.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(14,27,49.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(15,28,0.00,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(16,31,99.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(17,32,0.00,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(18,33,99.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(19,35,0.00,1,'2014-12-06 21:51:43','2014-12-06 21:51:43'),(20,34,149.99,1,'2014-12-06 21:51:43','2014-12-06 21:51:43');
/*!40000 ALTER TABLE `accounts_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add person',7,'add_person'),(20,'Can change person',7,'change_person'),(21,'Can delete person',7,'delete_person'),(22,'Can add organization',8,'add_organization'),(23,'Can change organization',8,'change_organization'),(24,'Can delete organization',8,'delete_organization'),(25,'Can add client',9,'add_client'),(26,'Can change client',9,'change_client'),(27,'Can delete client',9,'delete_client'),(28,'Can add subscription',10,'add_subscription'),(29,'Can change subscription',10,'change_subscription'),(30,'Can delete subscription',10,'delete_subscription'),(31,'Can add hosting account',11,'add_hostingaccount'),(32,'Can change hosting account',11,'change_hostingaccount'),(33,'Can delete hosting account',11,'delete_hostingaccount'),(39,'Can delete vendor',13,'delete_vendor'),(38,'Can change vendor',13,'change_vendor'),(37,'Can add vendor',13,'add_vendor'),(40,'Can add project',14,'add_project'),(41,'Can change project',14,'change_project'),(42,'Can delete project',14,'delete_project'),(43,'Can add project status',15,'add_projectstatus'),(44,'Can change project status',15,'change_projectstatus'),(45,'Can delete project status',15,'delete_projectstatus'),(46,'Can add project component',16,'add_projectcomponent'),(47,'Can change project component',16,'change_projectcomponent'),(48,'Can delete project component',16,'delete_projectcomponent'),(49,'Can add affiliation',17,'add_affiliation'),(50,'Can change affiliation',17,'change_affiliation'),(51,'Can delete affiliation',17,'delete_affiliation'),(52,'Can add work log',18,'add_worklog'),(53,'Can change work log',18,'change_worklog'),(54,'Can delete work log',18,'delete_worklog'),(55,'Can add task',19,'add_task'),(56,'Can change task',19,'change_task'),(57,'Can delete task',19,'delete_task');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (2,'pbkdf2_sha256$12000$Lco7ZUzXzTo7$4leXj5xfbF/4qV/kjLc0n1NfP6sPWPWu6eCel7jOdE4=','2014-12-22 23:51:00',0,'omar','','','',1,1,'2014-12-21 02:02:32'),(3,'pbkdf2_sha256$12000$3mfOb3EdKWfu$wiqjFF0wSD0Bp9x7PHTJ+A22AIekwH01DNY4ZTODZwk=','2015-01-05 18:29:03',1,'joshua','','','',1,1,'2014-12-21 02:02:45'),(4,'pbkdf2_sha256$12000$xS5d8kyawiza$9A5oDvbivkG6M+xVvdyOgauA6gPnTsXWd2R7Xdhfphw=','2014-12-31 07:46:15',1,'ryan','','','',1,1,'2014-12-21 02:03:09'),(5,'pbkdf2_sha256$12000$vgY4fWQw5Hiy$gjUTu3JBxSDkQYFTtfvjY4Yws+dkYNf89kBPhbDOcRY=','2014-12-26 19:24:55',0,'dylan','','','',1,1,'2014-12-26 19:24:55'),(6,'pbkdf2_sha256$12000$JpXfqP7tDAHI$4QgYLiKknoYYEruV7BPU8aJF9yzkpRYxvMNEBvaa7gk=','2015-01-02 22:53:59',0,'jessa','','','',1,1,'2014-12-31 18:50:51');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (1,2,32),(46,6,23),(45,6,54),(44,6,53),(43,6,52),(42,6,24),(41,6,48),(40,6,47),(39,6,46),(38,6,45),(37,6,44),(36,6,43),(35,6,42),(34,6,41),(33,6,40),(32,6,22),(47,6,56),(48,6,57),(49,6,25),(50,6,27),(51,6,26),(52,6,55);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=249 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (11,'2014-12-21 03:12:17','51','missn365',2,'Changed ip.',11,2),(10,'2014-12-21 03:12:17','52','myezbusi',2,'Changed ip.',11,2),(9,'2014-12-21 03:12:17','53','oilinyou',2,'Changed ip.',11,2),(8,'2014-12-21 03:12:17','54','shekgard',2,'Changed ip.',11,2),(7,'2014-12-21 02:03:41','1','admin',3,'',4,3),(12,'2014-12-21 03:12:17','50','mastermy',2,'Changed ip.',11,2),(13,'2014-12-21 03:12:17','49','kpiincco',2,'Changed ip.',11,2),(14,'2014-12-21 03:12:17','48','danamebc',2,'Changed ip.',11,2),(15,'2014-12-21 03:12:17','47','consilco',2,'Changed ip.',11,2),(16,'2014-12-21 03:12:17','46','rblevins',2,'Changed ip.',11,2),(17,'2014-12-21 03:12:17','45','writerss',2,'Changed ip.',11,2),(18,'2014-12-21 03:12:17','44','wanderdo',2,'Changed ip.',11,2),(19,'2014-12-21 03:12:17','43','villagec',2,'Changed ip.',11,2),(20,'2014-12-21 03:12:17','42','wwwutemu',2,'Changed ip.',11,2),(21,'2014-12-21 03:12:17','41','wwwunite',2,'Changed ip.',11,2),(22,'2014-12-21 03:12:17','40','wwwtier2',2,'Changed ip.',11,2),(23,'2014-12-21 03:12:17','39','tiercom',2,'Changed ip.',11,2),(24,'2014-12-21 03:12:17','38','wwwtheko',2,'Changed ip.',11,2),(25,'2014-12-21 03:12:17','37','techimag',2,'Changed ip.',11,2),(26,'2014-12-21 03:12:17','36','wwwstrat',2,'Changed ip.',11,2),(27,'2014-12-21 03:12:17','35','smallbiz',2,'Changed ip.',11,2),(28,'2014-12-21 03:12:17','34','wwwryand',2,'Changed ip.',11,2),(29,'2014-12-21 03:12:17','33','ronmetta',2,'Changed ip.',11,2),(30,'2014-12-21 03:12:17','32','alipasku',2,'Changed ip.',11,2),(31,'2014-12-21 03:12:17','31','wwwplaza',2,'Changed ip.',11,2),(32,'2014-12-21 03:12:17','28','networki',2,'Changed ip.',11,2),(33,'2014-12-21 03:12:17','26','wwwmissionbowlin',2,'Changed ip.',11,2),(34,'2014-12-21 03:12:17','25','maryland',2,'Changed ip.',11,2),(35,'2014-12-21 03:12:17','20','joshuabk',2,'Changed ip.',11,2),(36,'2014-12-21 03:12:17','19','instantr',2,'Changed ip.',11,2),(37,'2014-12-21 03:12:17','18','ggarnes',2,'Changed ip.',11,2),(38,'2014-12-21 03:12:17','17','getreadytoprofit',2,'Changed ip.',11,2),(39,'2014-12-21 03:12:17','16','booklovers',2,'Changed ip.',11,2),(40,'2014-12-21 03:12:17','15','wwwfidir',2,'Changed ip.',11,2),(41,'2014-12-21 03:12:17','14','eventsca',2,'Changed ip.',11,2),(42,'2014-12-21 03:12:17','13','environmentalser',2,'Changed ip.',11,2),(43,'2014-12-21 03:12:17','12','devtier2',2,'Changed ip.',11,2),(44,'2014-12-21 03:12:17','11','wwwcourt',2,'Changed ip.',11,2),(45,'2014-12-21 03:12:17','10','consumer',2,'Changed ip.',11,2),(46,'2014-12-21 03:12:17','9','chewchest',2,'Changed ip.',11,2),(47,'2014-12-21 03:12:17','7','rlmohl2',2,'Changed ip.',11,2),(48,'2014-12-21 03:12:17','6','wwwcalvertpearso',2,'Changed ip.',11,2),(49,'2014-12-21 03:12:17','5','boldnewe',2,'Changed ip.',11,2),(50,'2014-12-21 03:12:17','4','wwwalleg',2,'Changed email_accounts and ip.',11,2),(51,'2014-12-21 03:12:17','3','wwwalert',2,'Changed ip.',11,2),(52,'2014-12-21 03:12:17','2','alertinf',2,'Changed ip.',11,2),(53,'2014-12-21 03:12:17','1','abundant',2,'Changed ip.',11,2),(54,'2014-12-21 03:12:51','11','wwwcourt',2,'Changed email_accounts.',11,2),(55,'2014-12-21 03:14:04','26','wwwmissionbowlin',2,'Changed email_accounts.',11,2),(56,'2014-12-21 03:14:59','36','wwwstrat',2,'Changed email_accounts.',11,2),(57,'2014-12-21 03:16:25','41','wwwunite',2,'Changed email_accounts.',11,2),(58,'2014-12-21 03:16:25','40','wwwtier2',2,'Changed email_accounts.',11,2),(59,'2014-12-21 03:29:54','54','shekgard',2,'Changed ip.',11,2),(60,'2014-12-21 03:29:54','53','oilinyou',2,'Changed ip.',11,2),(61,'2014-12-21 03:29:54','52','myezbusi',2,'Changed ip.',11,2),(62,'2014-12-21 03:29:54','51','missn365',2,'Changed ip.',11,2),(63,'2014-12-21 03:29:54','50','mastermy',2,'Changed ip.',11,2),(64,'2014-12-21 03:29:54','49','kpiincco',2,'Changed ip.',11,2),(65,'2014-12-21 03:29:54','48','danamebc',2,'Changed ip.',11,2),(66,'2014-12-21 03:29:54','47','consilco',2,'Changed ip.',11,2),(67,'2014-12-21 03:29:54','44','wanderdo',2,'Changed ip.',11,2),(68,'2014-12-21 03:29:54','43','villagec',2,'Changed ip.',11,2),(69,'2014-12-21 03:29:54','42','wwwutemu',2,'Changed ip.',11,2),(70,'2014-12-21 03:29:54','38','wwwtheko',2,'Changed ip.',11,2),(71,'2014-12-21 03:29:54','34','wwwryand',2,'Changed ip.',11,2),(72,'2014-12-21 03:29:54','33','ronmetta',2,'Changed ip.',11,2),(73,'2014-12-21 03:29:54','31','wwwplaza',2,'Changed ip.',11,2),(74,'2014-12-21 03:29:54','25','maryland',2,'Changed ip.',11,2),(75,'2014-12-21 03:29:54','20','joshuabk',2,'Changed ip.',11,2),(76,'2014-12-21 03:29:54','19','instantr',2,'Changed ip.',11,2),(77,'2014-12-21 03:29:54','17','getreadytoprofit',2,'Changed ip.',11,2),(78,'2014-12-21 03:29:54','16','booklovers',2,'Changed ip.',11,2),(79,'2014-12-21 03:29:54','15','wwwfidir',2,'Changed ip.',11,2),(80,'2014-12-21 03:29:54','14','eventsca',2,'Changed ip.',11,2),(81,'2014-12-21 03:29:54','13','environmentalser',2,'Changed ip.',11,2),(82,'2014-12-21 03:29:54','9','chewchest',2,'Changed ip.',11,2),(83,'2014-12-21 03:29:54','6','wwwcalvertpearso',2,'Changed ip.',11,2),(84,'2014-12-21 03:29:54','5','boldnewe',2,'Changed ip.',11,2),(85,'2014-12-21 03:29:54','3','wwwalert',2,'Changed ip.',11,2),(86,'2014-12-21 03:29:54','2','alertinf',2,'Changed ip.',11,2),(87,'2014-12-21 03:29:54','1','abundant',2,'Changed ip.',11,2),(88,'2014-12-21 03:29:54','45','writerss',2,'Changed ip.',11,2),(89,'2014-12-21 03:29:54','41','wwwunite',2,'Changed ip.',11,2),(90,'2014-12-21 03:29:54','35','smallbiz',2,'Changed ip.',11,2),(91,'2014-12-21 03:29:54','18','ggarnes',2,'Changed ip.',11,2),(92,'2014-12-21 03:29:54','12','devtier2',2,'Changed ip.',11,2),(93,'2014-12-21 03:29:54','11','wwwcourt',2,'Changed ip.',11,2),(94,'2014-12-21 03:29:54','36','wwwstrat',2,'Changed ip.',11,2),(95,'2014-12-21 03:29:54','7','rlmohl2',2,'Changed ip.',11,2),(96,'2014-12-21 03:29:54','37','techimag',2,'Changed ip.',11,2),(97,'2014-12-21 03:29:54','10','consumer',2,'Changed ip.',11,2),(98,'2014-12-21 03:29:54','32','alipasku',2,'Changed ip.',11,2),(99,'2014-12-21 03:29:54','28','networki',2,'Changed ip.',11,2),(100,'2014-12-21 03:29:54','40','wwwtier2',2,'Changed ip.',11,2),(101,'2014-12-21 03:29:54','4','wwwalleg',2,'Changed ip.',11,2),(102,'2014-12-21 03:29:54','46','rblevins',2,'Changed ip.',11,2),(103,'2014-12-21 03:29:54','26','wwwmissionbowlin',2,'Changed ip.',11,2),(104,'2014-12-21 03:29:54','39','tiercom',2,'Changed ip.',11,2),(105,'2014-12-22 04:08:09','44','wanderdo',2,'Changed ip.',11,2),(106,'2014-12-22 04:08:09','42','wwwutemu',2,'Changed site_down.',11,2),(107,'2014-12-22 04:08:09','38','wwwtheko',2,'Changed ip.',11,2),(108,'2014-12-22 04:08:09','33','ronmetta',2,'Changed site_compromised.',11,2),(109,'2014-12-22 04:08:09','31','wwwplaza',2,'Changed site_down.',11,2),(110,'2014-12-22 04:08:09','25','maryland',2,'Changed ip.',11,2),(111,'2014-12-22 04:08:09','20','joshuabk',2,'Changed ip.',11,2),(112,'2014-12-22 04:08:09','17','getreadytoprofit',2,'Changed ip.',11,2),(113,'2014-12-22 04:08:09','16','booklovers',2,'Changed ip.',11,2),(114,'2014-12-22 04:08:09','9','chewchest',2,'Changed ip.',11,2),(115,'2014-12-22 04:08:09','6','wwwcalvertpearso',2,'Changed ip.',11,2),(116,'2014-12-22 04:08:09','5','boldnewe',2,'Changed ip.',11,2),(117,'2014-12-22 04:08:09','3','wwwalert',2,'Changed site_down.',11,2),(118,'2014-12-22 04:08:09','2','alertinf',2,'Changed site_down.',11,2),(119,'2014-12-22 04:08:09','1','abundant',2,'Changed ip.',11,2),(120,'2014-12-22 04:09:55','34','wwwryand',2,'Changed ip.',11,2),(121,'2014-12-22 23:50:46','2','omar',2,'Changed is_superuser and user_permissions.',4,3),(122,'2014-12-23 16:22:12','1','Project object',1,'',14,3),(123,'2014-12-23 16:22:54','1','Learning Management System',2,'Changed project_code.',14,3),(124,'2014-12-23 16:25:57','1','Learning Management System',2,'Changed value.',14,3),(125,'2014-12-23 16:28:26','2','Aspen Strong Directory',1,'',14,3),(126,'2014-12-23 16:28:41','3','Tokome',1,'',14,3),(127,'2014-12-23 16:31:57','4','Westcon Directory',1,'',14,3),(128,'2014-12-23 16:48:52','4','Westcon Directory',2,'Changed status.',14,3),(129,'2014-12-23 16:50:48','5','AWARE',1,'',14,3),(130,'2014-12-23 16:55:40','5','AWARE',2,'Added project component \"Phase 1\". Added project component \"Phase 2\". Added project component \"Phase 3\". Added project component \"Phase 4\".',14,3),(131,'2014-12-23 16:55:59','4','Westcon Directory',2,'Added project component \"Phase 1\". Added project component \"Phase 2\".',14,3),(132,'2014-12-23 16:57:33','3','Tokome',2,'Added project component \"Phase 1\". Added project component \"Phase 2\". Added project component \"Phase 3\". Added project component \"Phase 4\".',14,3),(133,'2014-12-23 16:58:54','2','Aspen Strong Directory',2,'Added project component \"Initial Form Extension\". Added project component \" Category-Keyword Pairing\". Added project component \"Issue/Mental Health Diagnosis Dropdowns\". Added project component \"Filters\".',14,3),(134,'2014-12-23 17:01:30','2','Aspen Strong Directory',2,'Added project component \"Conditionally hide credentials\". Added project component \"Change options on landing page\". Added project component \"Limit personal statement\". Added project component \"Prevent users from entering the WordPress admin\". Added project component \"Clear initial search results\". Added project component \"Extend specialty functionality\". Added project component \"Allow front end form editing\". Added project component \"Mandate all fields\". Added project component \"Design single entry page\". Changed name for project component \"Initial form extension\". Changed name for project component \" Category-Keyword pairing\". Changed name for project component \"Issue/Mental Health Diagnosis dropdowns\".',14,3),(135,'2014-12-23 17:04:26','1','Learning Management System',2,'Added project component \"Retainer\". Added project component \"Phase 1\". Added project component \"Phase 2\". Added project component \"Phase 3\". Added project component \"Phase 4\". Added project component \"Phase 5\". Added project component \"Phase 6\". Added project component \"Phase 7\". Added project component \"Phase 8\".',14,3),(136,'2014-12-23 17:07:19','1','Learning Management System',2,'Changed value for project component \"Phase 8\".',14,3),(137,'2014-12-23 17:16:19','6','Reservation System',1,'',14,3),(138,'2014-12-23 17:24:48','7','Mission Bowling Club Website',1,'',14,3),(139,'2014-12-23 17:25:55','8','Sex Love',1,'',14,3),(140,'2014-12-23 17:54:32','9','LiveAha',1,'',14,3),(141,'2014-12-23 18:01:16','10','Forester Website',1,'',14,3),(142,'2014-12-23 18:02:02','11','LazyCoin',1,'',14,3),(143,'2014-12-23 18:03:17','12','ArrowTech',1,'',14,3),(144,'2014-12-23 18:19:18','12','ArrowTech',2,'No fields changed.',14,3),(145,'2014-12-23 18:20:14','12','ArrowTech',2,'No fields changed.',14,3),(146,'2014-12-23 18:20:20','12','ArrowTech',2,'Changed status for project component \"Phase 3\".',14,3),(147,'2014-12-23 18:20:26','12','ArrowTech',2,'Changed status for project component \"Phase 3\".',14,3),(148,'2014-12-23 18:20:32','11','LazyCoin',2,'No fields changed.',14,3),(149,'2014-12-23 18:20:49','11','LazyCoin',2,'No fields changed.',14,3),(150,'2014-12-23 18:22:51','11','LazyCoin',2,'No fields changed.',14,3),(151,'2014-12-23 18:22:56','10','Forester Website',2,'No fields changed.',14,3),(152,'2014-12-23 18:23:01','9','LiveAha',2,'No fields changed.',14,3),(153,'2014-12-23 18:23:04','8','Sex Love',2,'No fields changed.',14,3),(154,'2014-12-23 18:23:12','7','Mission Bowling Club Website',2,'No fields changed.',14,3),(155,'2014-12-23 18:23:21','6','Reservation System',2,'Changed value for project component \"Bug fixes\".',14,3),(156,'2014-12-23 18:23:30','3','Tokome',2,'No fields changed.',14,3),(157,'2014-12-23 18:23:35','2','Aspen Strong Directory',2,'No fields changed.',14,3),(158,'2014-12-23 18:23:45','1','Learning Management System',2,'No fields changed.',14,3),(159,'2014-12-23 18:26:08','13','Site merging',1,'',14,3),(160,'2014-12-23 18:27:35','13','Site Maintenance',2,'Changed name and code. Added project component \"WordPress Updates\".',14,3),(161,'2014-12-23 18:28:00','14','AbundiaNow',1,'',14,3),(162,'2014-12-24 19:32:58','12','ArrowTech',2,'Changed status for project component \"Phase 1\".',14,4),(163,'2014-12-24 19:33:25','12','ArrowTech',2,'Changed billed for project component \"Phase 1\".',14,4),(164,'2014-12-24 19:34:09','14','AbundiaNow',2,'Changed billed and paid for project component \"Development\".',14,3),(165,'2014-12-25 03:13:19','56','Julie Ringnes',1,'',8,3),(166,'2014-12-25 20:42:16','40','Julie Ringnes',1,'',9,3),(167,'2014-12-25 20:42:30','14','AbundiaNow',2,'Changed client.',14,3),(168,'2014-12-25 20:42:39','13','Site Maintenance',2,'Changed client.',14,3),(169,'2014-12-25 20:42:47','12','ArrowTech',2,'Changed client.',14,3),(170,'2014-12-25 20:43:05','57','LazyCoin',1,'',8,3),(171,'2014-12-25 20:43:08','41','LazyCoin',1,'',9,3),(172,'2014-12-25 20:43:37','11','LazyCoin',2,'Changed client.',14,3),(173,'2014-12-25 20:43:46','10','Forester Website',2,'Changed client.',14,3),(174,'2014-12-25 20:44:13','9','LiveAha',2,'Changed client.',14,3),(175,'2014-12-25 20:44:31','8','Sex Love',2,'Changed client.',14,3),(176,'2014-12-25 20:44:39','7','Mission Bowling Club Website',2,'Changed client.',14,3),(177,'2014-12-25 20:44:46','6','Reservation System',2,'Changed client.',14,3),(178,'2014-12-25 20:44:49','6','Reservation System',2,'No fields changed.',14,3),(179,'2014-12-25 20:45:00','5','AWARE',2,'Changed client.',14,3),(180,'2014-12-25 20:45:15','4','Westcon Directory',2,'Changed client.',14,3),(181,'2014-12-25 20:45:24','3','Tokome',2,'Changed client.',14,3),(182,'2014-12-25 20:45:29','2','Aspen Strong Directory',2,'No fields changed.',14,3),(183,'2014-12-25 20:45:42','1','Learning Management System',2,'Changed client.',14,3),(184,'2014-12-25 20:59:23','57','LazyCoin',2,'Added affiliation \"Affiliation object\".',8,3),(185,'2014-12-25 20:59:31','56','Julie Ringnes',2,'Added affiliation \"Affiliation object\".',8,3),(186,'2014-12-25 21:00:59','33','Manhattan Residential Group',2,'Deleted affiliation \"Affiliation object\".',8,3),(187,'2014-12-25 21:01:11','32','Environmental Services Marketplace',2,'Deleted affiliation \"Affiliation object\".',8,3),(188,'2014-12-25 21:06:13','58','Capital Connections',1,'',8,3),(189,'2014-12-25 21:06:18','53','Michael Douglass',1,'',7,3),(190,'2014-12-25 21:06:36','42','Capital Connections',1,'',9,3),(191,'2014-12-25 21:07:15','15','Capital Connections',1,'',14,3),(192,'2014-12-25 21:10:03','16','RFP Trainer',1,'',14,3),(193,'2014-12-25 21:19:48','17','Land of Possibility',1,'',14,3),(194,'2014-12-25 22:52:35','54','shekgard',2,'Changed email_accounts.',11,3),(195,'2014-12-25 22:54:32','52','myezbusi',2,'Changed email_accounts.',11,3),(196,'2014-12-25 22:54:32','51','missn365',2,'Changed email_accounts.',11,3),(197,'2014-12-25 22:54:32','50','mastermy',2,'Changed email_accounts.',11,3),(198,'2014-12-25 22:54:32','49','kpiincco',2,'Changed email_accounts.',11,3),(199,'2014-12-25 22:54:32','48','danamebc',2,'Changed email_accounts.',11,3),(200,'2014-12-25 22:54:32','47','consilco',2,'Changed email_accounts.',11,3),(201,'2014-12-25 23:01:00','18','Yorktowne Sports',1,'',14,3),(202,'2014-12-26 17:38:24','19','Glenn Garnes',1,'',14,3),(203,'2014-12-26 18:50:13','27','melissa',2,'Changed ip.',11,3),(204,'2014-12-26 18:58:45','43','villagec',2,'Changed email_accounts.',11,3),(205,'2014-12-26 19:16:40','12','ArrowTech',2,'Changed billed for project component \"Phase 1\".',14,3),(206,'2014-12-26 19:24:41','19','Glenn Garnes',2,'Changed manager.',14,3),(207,'2014-12-26 19:24:41','18','Yorktowne Sports',2,'Changed manager.',14,3),(208,'2014-12-26 19:24:41','17','Land of Possibility',2,'Changed manager.',14,3),(209,'2014-12-26 19:24:41','16','RFP Trainer',2,'Changed manager.',14,3),(210,'2014-12-26 19:24:55','5','dylan',1,'',4,3),(211,'2014-12-26 19:25:04','5','dylan',2,'Changed is_staff.',4,3),(212,'2014-12-26 19:25:34','15','Capital Connections',2,'Changed manager.',14,3),(213,'2014-12-26 19:25:34','14','AbundiaNow',2,'Changed manager.',14,3),(214,'2014-12-26 19:25:35','13','Site Maintenance',2,'Changed manager.',14,3),(215,'2014-12-26 19:25:35','12','ArrowTech',2,'Changed manager.',14,3),(216,'2014-12-26 19:25:35','11','LazyCoin',2,'Changed manager.',14,3),(217,'2014-12-26 19:25:35','10','Forester Website',2,'Changed manager.',14,3),(218,'2014-12-26 19:25:35','9','LiveAha',2,'Changed manager.',14,3),(219,'2014-12-26 19:25:35','8','Sex Love',2,'Changed manager.',14,3),(220,'2014-12-26 19:26:07','7','Mission Bowling Club Website',2,'Changed manager.',14,3),(221,'2014-12-26 19:26:07','6','Reservation System',2,'Changed manager.',14,3),(222,'2014-12-26 19:26:07','5','AWARE',2,'Changed manager.',14,3),(223,'2014-12-26 19:26:07','4','Westcon Directory',2,'Changed manager.',14,3),(224,'2014-12-26 19:26:07','2','Aspen Strong Directory',2,'Changed manager.',14,3),(225,'2014-12-26 19:33:41','17','Land of Possibility',2,'Changed priority.',14,3),(226,'2014-12-26 19:33:41','12','ArrowTech',2,'Changed priority.',14,3),(227,'2014-12-26 19:33:41','11','LazyCoin',2,'Changed priority.',14,3),(228,'2014-12-26 19:33:41','9','LiveAha',2,'Changed priority.',14,3),(229,'2014-12-26 19:33:41','7','Mission Bowling Club Website',2,'Changed priority.',14,3),(230,'2014-12-26 19:33:41','6','Reservation System',2,'Changed priority.',14,3),(231,'2014-12-26 19:33:41','5','AWARE',2,'Changed priority.',14,3),(232,'2014-12-26 19:33:41','4','Westcon Directory',2,'Changed priority.',14,3),(233,'2014-12-26 19:33:41','3','Tokome',2,'Changed priority.',14,3),(234,'2014-12-26 19:33:41','2','Aspen Strong Directory',2,'Changed priority.',14,3),(235,'2014-12-26 19:33:41','1','Learning Management System',2,'Changed priority.',14,3),(236,'2014-12-26 19:36:06','20','Server Migration',1,'',14,3),(237,'2014-12-26 19:36:17','2','Aspen Strong Directory',2,'Changed priority.',14,3),(238,'2014-12-26 21:02:20','21','ConnectDG',1,'',14,4),(239,'2014-12-26 21:02:28','21','ConnectDG',2,'Changed priority.',14,4),(240,'2014-12-26 21:03:36','21','ConnectDG',2,'Added project component \"ConnectDG\".',14,4),(241,'2014-12-30 01:03:43','55','Govcon',1,'',11,3),(242,'2014-12-31 18:50:51','6','jessa',1,'',4,3),(243,'2014-12-31 18:51:46','6','jess',2,'Changed username and user_permissions.',4,3),(244,'2014-12-31 18:52:37','6','jessa',2,'Changed username and is_staff.',4,3),(245,'2015-01-02 22:52:47','6','jessa',2,'Changed user_permissions.',4,3),(246,'2015-01-02 23:00:47','14','AbundiaNow',2,'Changed notes. Added task \"Task object\". Added task \"Task object\". Added task \"Task object\".',14,6),(247,'2015-01-02 23:05:42','14','AbundiaNow',2,'Changed person for task \"Install the plugins\". Changed person for task \"Point the domain to our server\r\n\".',14,6),(248,'2015-01-02 23:06:51','4','Program project link on Tasks view to link to the project, not the task',1,'',19,6);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'person','people','person'),(8,'organization','people','organization'),(9,'client','people','client'),(10,'subscription','accounts','subscription'),(11,'hosting account','accounts','hostingaccount'),(13,'vendor','people','vendor'),(14,'project','projects','project'),(15,'project status','projects','projectstatus'),(16,'project component','projects','projectcomponent'),(17,'affiliation','people','affiliation'),(18,'work log','projects','worklog'),(19,'task','projects','task');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'people','0001_initial','2014-12-21 01:46:01'),(2,'people','0002_auto_20141221_0100','2014-12-21 01:46:01'),(3,'people','0003_auto_20141221_0110','2014-12-21 01:46:02'),(4,'people','0004_auto_20141221_0112','2014-12-21 01:46:02'),(5,'people','0005_auto_20141221_0114','2014-12-21 01:46:02'),(6,'people','0006_auto_20141221_0114','2014-12-21 01:46:02'),(7,'people','0007_client','2014-12-21 01:46:02'),(8,'people','0008_auto_20141221_0118','2014-12-21 01:46:03'),(9,'people','0009_client','2014-12-21 01:46:03'),(10,'people','0010_auto_20141221_0130','2014-12-21 01:46:03'),(11,'accounts','0001_initial','2014-12-21 01:48:54'),(12,'contenttypes','0001_initial','2014-12-21 01:48:54'),(13,'auth','0001_initial','2014-12-21 01:48:55'),(14,'admin','0001_initial','2014-12-21 01:48:55'),(15,'sessions','0001_initial','2014-12-21 01:48:55'),(16,'accounts','0002_hostingaccount','2014-12-21 01:49:33'),(17,'people','0011_vendor','2014-12-21 02:00:00'),(18,'people','0012_auto_20141221_0200','2014-12-21 02:00:35'),(19,'people','0013_vendor','2014-12-21 02:00:57'),(20,'accounts','0003_auto_20141222_0231','2014-12-22 02:31:35'),(21,'projects','0001_initial','2014-12-23 16:21:50'),(22,'projects','0002_project_project_code','2014-12-23 16:22:48'),(23,'projects','0003_auto_20141223_1625','2014-12-23 16:25:08'),(24,'projects','0004_auto_20141223_1633','2014-12-23 16:34:00'),(25,'projects','0005_projectcomponent','2014-12-23 16:53:58'),(26,'projects','0006_remove_project_value','2014-12-23 17:16:50'),(27,'projects','0007_project_client','2014-12-25 03:10:34'),(28,'projects','0008_auto_20141225_0311','2014-12-25 03:11:28'),(29,'people','0014_affiliation','2014-12-25 03:16:03'),(30,'people','0015_auto_20141225_0316','2014-12-25 03:16:50'),(31,'people','0016_auto_20141225_0317','2014-12-25 03:17:17'),(32,'people','0017_remove_affiliation_context','2014-12-25 03:17:29'),(33,'people','0018_affiliation_context','2014-12-25 03:17:51'),(34,'people','0019_auto_20141225_0318','2014-12-25 03:18:12'),(35,'people','0020_auto_20141225_2042','2014-12-25 20:42:15'),(36,'accounts','0004_auto_20141226_1849','2014-12-26 18:50:01'),(37,'projects','0009_project_manager','2014-12-26 19:19:58'),(38,'projects','0010_auto_20141226_1921','2014-12-26 19:21:07'),(39,'projects','0011_auto_20141226_1931','2014-12-26 19:31:08'),(40,'projects','0012_auto_20141226_1931','2014-12-26 19:31:58'),(41,'projects','0013_remove_project_priority','2014-12-26 19:31:58'),(42,'projects','0014_project_priority','2014-12-26 19:32:07'),(43,'projects','0015_project_notes','2015-01-02 22:37:12'),(44,'projects','0016_project_due','2015-01-02 22:40:48'),(45,'projects','0017_worklog','2015-01-02 22:49:00'),(46,'projects','0018_task','2015-01-02 22:51:40'),(47,'projects','0019_task_project','2015-01-02 22:53:40'),(48,'projects','0020_auto_20150102_2256','2015-01-02 22:56:27'),(49,'projects','0021_task_complete','2015-01-02 22:57:24');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('dg6hja7afcesume0u89e98f1p2vekek4','NjJlM2RkMDAwZGNkNzY3M2VjZWY2OTU0ZDYyZDM2MTExMmY3NjFiZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImM1OTk5YmZmZWQ4ZDI5NmE4YWZlZjE3ZGRkMDVkODYwMDNmNmI5ZTAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9','2015-01-04 03:09:12'),('g2tlz30uak16ea6b2y5puey1bzrnt61a','NjJlM2RkMDAwZGNkNzY3M2VjZWY2OTU0ZDYyZDM2MTExMmY3NjFiZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImM1OTk5YmZmZWQ4ZDI5NmE4YWZlZjE3ZGRkMDVkODYwMDNmNmI5ZTAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9','2015-01-04 02:03:48'),('gckggd75n45plmafe5qfqha5plzbzk46','NjJlM2RkMDAwZGNkNzY3M2VjZWY2OTU0ZDYyZDM2MTExMmY3NjFiZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImM1OTk5YmZmZWQ4ZDI5NmE4YWZlZjE3ZGRkMDVkODYwMDNmNmI5ZTAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9','2015-01-05 02:33:57'),('apaar8zhuypvbvfv8hef0w5dq9c812n7','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-05 02:03:18'),('etdepqlucpvpiwv7opfwrnzvko9mvdd4','MTlhMmIxOWU3YjM5NjE1ZjBjZDQyM2YxM2Q4NTA1OGJkZGY0YzI0Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjFiMTkwZjI1Nzc0MjgzM2IzY2U2ZTUwYzAwY2FhYWI1MjEyZTdiYmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjR9','2015-01-07 19:10:21'),('eiaionf0nilokqr9sgjbzvkm6oy98aey','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-05 23:51:18'),('4zbfzo5us2jc4ao1mi9qzrgrzf1ia0lg','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-06 16:19:23'),('lh016mnrgmvrqkj7895ijwxtwkmc8hhz','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-06 17:12:55'),('plzckm10zrfs69yi48rmujy46j2vt06q','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-07 19:09:56'),('30bu8urb4egvqi7h3c51jucg79kcn6vj','MDhjNmM2NTlhYTgwM2YzNTA2ZWM1ZjUxOTY4OTY5M2I4ZmY2NjJiNDp7fQ==','2015-01-08 03:07:45'),('qsoxk4yigxs355s62bsljufuz3j5jhdz','MDhjNmM2NTlhYTgwM2YzNTA2ZWM1ZjUxOTY4OTY5M2I4ZmY2NjJiNDp7fQ==','2015-01-08 20:40:12'),('dq034potts2nqi8sgjuswx88a371ba32','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 03:07:52'),('9geky4lftj9q6220mqtn3n4d6a32b9ol','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 20:40:16'),('menk7udt6xpzxgli3k6g3m4dkjsrsn9a','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-08 22:52:27'),('x09m1tkgubcy3klkg10p4psjscfco2zu','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-09 16:52:14'),('agiyku3pczsoy5w2go73uexulx60g4cl','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-09 18:41:54'),('6az9bvlqta5tulk8i54ot5d65p5smxj4','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-09 20:54:42'),('hv9zo2bxqfnrtlnufmokw0xlcmjy0t3h','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-12 03:52:51'),('fl2p5xu79qrtm73amjxy037yz1ivsfwg','MDQxMDM0YTMzOGJlYTNmZDAyMTk2YWJiZDZkMjQ1Y2Y4MTk2MzU2Njp7Il9hdXRoX3VzZXJfaGFzaCI6IjBlZjYxYzAyMDg0ZjNiYjE2ZTRiMmY3ZTZhMzJlY2U0NGNmZTYyMjUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9','2015-01-16 22:25:07'),('qg7qcl1qgkjiwp6abzgyljln07pj5qb5','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-13 00:17:38'),('j674myyiaxfnrcf70215wta3graweowq','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-13 22:46:27'),('8rg1dcjrjt14gvxiq3vtz0nyay7w1nrg','MTlhMmIxOWU3YjM5NjE1ZjBjZDQyM2YxM2Q4NTA1OGJkZGY0YzI0Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjFiMTkwZjI1Nzc0MjgzM2IzY2U2ZTUwYzAwY2FhYWI1MjEyZTdiYmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjR9','2015-01-14 07:46:15'),('dq1co8t86881xl28g6gv14mwds56k5x2','MDQxMDM0YTMzOGJlYTNmZDAyMTk2YWJiZDZkMjQ1Y2Y4MTk2MzU2Njp7Il9hdXRoX3VzZXJfaGFzaCI6IjBlZjYxYzAyMDg0ZjNiYjE2ZTRiMmY3ZTZhMzJlY2U0NGNmZTYyMjUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjZ9','2015-01-14 18:52:44'),('ja0aogh44rm49lru9zkkiiqj068eq0yb','NzIyNzI4ZGE5ZDdjNTQ3MzI2MTRjMDljZDllMWM2YjBlNzZiNThiOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzMjRjMTA4MTJkN2ExYTY4N2EzNmJmNjY5N2FlZDFhOWRiNDdkOWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjN9','2015-01-19 18:29:03');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_affiliation`
--

DROP TABLE IF EXISTS `people_affiliation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_affiliation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `context` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_affiliation_26b2345e` (`organization_id`),
  KEY `people_affiliation_a8452ca7` (`person_id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_affiliation`
--

LOCK TABLES `people_affiliation` WRITE;
/*!40000 ALTER TABLE `people_affiliation` DISABLE KEYS */;
INSERT INTO `people_affiliation` VALUES (1,1,1,'Member','2014-12-06 21:51:39','2014-12-06 21:51:39'),(2,2,3,'Owner','2014-12-06 21:51:39','2014-12-06 21:51:39'),(3,3,4,'Owner','2014-12-06 21:51:39','2014-12-06 21:51:39'),(4,3,5,'Owner','2014-12-06 21:51:39','2014-12-06 21:51:39'),(5,3,6,'Owner','2014-12-06 21:51:39','2014-12-06 21:51:39'),(6,3,7,'Associate','2014-12-06 21:51:39','2014-12-06 21:51:39'),(7,4,8,'Owner','2014-12-06 21:51:39','2014-12-06 21:51:39'),(8,4,10,'Associate','2014-12-06 21:51:39','2014-12-06 21:51:39'),(9,5,11,'Owner','2014-12-06 21:51:39','2014-12-06 21:51:39'),(10,5,12,'Employee','2014-12-06 21:51:39','2014-12-06 21:51:39'),(11,5,13,'Employee','2014-12-06 21:51:39','2014-12-06 21:51:39'),(12,6,14,'Employee','2014-12-06 21:51:39','2014-12-06 21:51:39'),(13,6,15,'Employee','2014-12-06 21:51:40','2014-12-06 21:51:40'),(14,7,2,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(15,8,16,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(16,9,17,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(17,9,18,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(18,9,19,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(19,10,20,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(20,11,21,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(21,12,22,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(22,13,23,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(23,13,24,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(24,14,25,'Owner','2014-12-06 21:51:40','2014-12-06 21:51:40'),(65,58,53,'President','2014-12-25 21:06:18','2014-12-25 21:06:18'),(27,28,26,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(28,29,27,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(29,30,28,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(31,31,29,NULL,'2014-12-06 21:51:40','2014-12-06 21:51:40'),(32,32,30,NULL,'2014-12-06 21:51:40','2014-12-06 21:51:40'),(33,33,31,NULL,'2014-12-06 21:51:40','2014-12-06 21:51:40'),(34,34,32,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(35,35,19,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(36,36,33,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(37,37,9,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(39,38,34,NULL,'2014-12-06 21:51:40','2014-12-06 21:51:40'),(41,39,16,NULL,'2014-12-06 21:51:40','2014-12-06 21:51:40'),(42,40,36,'Self','2014-12-06 21:51:40','2014-12-06 21:51:40'),(44,41,18,NULL,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(45,42,37,'Self','2014-12-06 21:51:41','2014-12-06 21:51:41'),(46,43,17,'Self','2014-12-06 21:51:41','2014-12-06 21:51:41'),(48,44,38,NULL,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(49,45,39,'Self','2014-12-06 21:51:41','2014-12-06 21:51:41'),(51,46,40,NULL,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(53,47,41,NULL,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(54,48,43,'Self','2014-12-06 21:51:41','2014-12-06 21:51:41'),(55,49,44,'Self','2014-12-06 21:59:32','2014-12-06 21:59:32'),(57,50,46,NULL,'2014-12-08 16:59:27','2014-12-08 16:59:27'),(59,51,47,'Owner','2014-12-09 21:22:24','2014-12-09 21:24:38'),(60,52,49,'Owner','2014-12-09 23:15:33','2014-12-09 23:16:07'),(61,52,50,'Technician','2014-12-09 23:35:20','2014-12-09 23:36:16'),(62,53,51,NULL,'2014-12-11 05:32:53','2014-12-11 05:32:53'),(63,57,52,'','2014-12-25 20:59:23','2014-12-25 20:59:23'),(64,56,45,'','2014-12-25 20:59:31','2014-12-25 20:59:31'),(66,59,55,'Self','2014-12-25 21:15:46','2014-12-25 21:15:46');
/*!40000 ALTER TABLE `people_affiliation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_client`
--

DROP TABLE IF EXISTS `people_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `relationship_status` decimal(3,2) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_client_26b2345e` (`organization_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_client`
--

LOCK TABLES `people_client` WRITE;
/*!40000 ALTER TABLE `people_client` DISABLE KEYS */;
INSERT INTO `people_client` VALUES (1,1,0.78,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(2,2,0.71,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(3,3,0.82,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(4,4,0.59,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(5,5,0.41,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(6,6,0.91,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(7,7,0.87,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(8,8,0.71,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(9,9,0.77,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(10,10,0.83,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(11,11,0.71,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(12,12,0.85,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(13,13,0.68,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(14,14,0.73,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(15,28,0.00,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(16,29,0.00,'2014-12-06 21:51:41','2014-12-06 21:51:41'),(17,30,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(18,31,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(19,32,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(20,33,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(21,34,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(22,35,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(23,36,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(24,37,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(25,38,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(26,39,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(27,40,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(28,41,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(29,42,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(30,43,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(31,44,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(32,45,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(33,46,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(34,47,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(35,48,0.00,'2014-12-06 21:51:42','2014-12-06 21:51:42'),(36,49,0.00,'2014-12-06 21:59:32','2014-12-06 21:59:32'),(37,50,0.00,'2014-12-08 16:58:01','2014-12-08 16:58:01'),(38,51,0.00,'2014-12-09 21:21:30','2014-12-09 21:21:30'),(39,52,0.00,'2014-12-09 23:14:40','2014-12-09 23:14:40'),(40,56,NULL,'2014-12-25 20:42:16','2014-12-25 20:42:16'),(41,57,NULL,'2014-12-25 20:43:08','2014-12-25 20:43:08'),(42,58,NULL,'2014-12-25 21:06:36','2014-12-25 21:06:36'),(43,59,NULL,'2014-12-25 21:15:46','2014-12-25 21:15:46');
/*!40000 ALTER TABLE `people_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_organization`
--

DROP TABLE IF EXISTS `people_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_organization`
--

LOCK TABLES `people_organization` WRITE;
/*!40000 ALTER TABLE `people_organization` DISABLE KEYS */;
INSERT INTO `people_organization` VALUES (1,'Ajax Technologies','2014-12-06 21:51:45','2014-12-06 21:51:45'),(2,'LaMagna & Associates','2014-12-06 21:51:45','2014-12-06 21:51:45'),(3,'Amor Innovations','2014-12-06 21:51:45','2014-12-06 21:51:45'),(4,'Bigger Pie Strategies','2014-12-06 21:51:45','2014-12-06 21:51:45'),(5,'Mission Bowling Club','2014-12-06 21:51:45','2014-12-06 21:51:45'),(6,'Orthodox Christian Fellowship','2014-12-06 21:51:45','2014-12-06 21:51:45'),(7,'Village Connector Media','2014-12-06 21:51:45','2014-12-06 21:51:45'),(8,'Tech Image Marketing','2014-12-06 21:51:45','2014-12-06 21:51:45'),(9,'Tier 27','2014-12-06 21:51:45','2014-12-06 21:51:45'),(10,'A-HA DESIGN','2014-12-06 21:51:45','2014-12-06 21:51:45'),(11,'Ti-promotion','2014-12-06 21:51:45','2014-12-06 21:51:45'),(12,'Virtuarte','2014-12-06 21:51:45','2014-12-06 21:51:45'),(13,'Allegheny Security','2014-12-06 21:51:45','2014-12-06 21:51:45'),(14,'Calvert Pearson','2014-12-06 21:51:45','2014-12-06 21:51:45'),(15,'Bank of America','2014-12-06 21:51:45','2014-12-06 21:51:45'),(16,'Trello','2014-12-06 21:51:45','2014-12-06 21:51:45'),(17,'Slack','2014-12-06 21:51:45','2014-12-06 21:51:45'),(18,'Google','2014-12-06 21:51:45','2014-12-06 21:51:45'),(19,'Harvest','2014-12-06 21:51:45','2014-12-06 21:51:45'),(20,'Basecamp','2014-12-06 21:51:45','2014-12-06 21:51:45'),(21,'A Small Orange','2014-12-06 21:51:45','2014-12-06 21:51:45'),(22,'Freshbooks','2014-12-06 21:51:45','2014-12-06 21:51:45'),(23,'GitHub','2014-12-06 21:51:45','2014-12-06 21:51:45'),(24,'Balsamiq','2014-12-06 21:51:45','2014-12-06 21:51:45'),(25,'OVH','2014-12-06 21:51:46','2014-12-06 21:51:46'),(26,'Timely','2014-12-06 21:51:46','2014-12-06 21:51:46'),(27,'Elance','2014-12-06 21:51:46','2014-12-06 21:51:46'),(28,'Ron Mohl','2014-12-06 21:51:46','2014-12-06 21:51:46'),(29,'Mia Reed','2014-12-06 21:51:46','2014-12-06 21:51:46'),(30,'Lisa Ridgley','2014-12-06 21:51:46','2014-12-06 21:51:46'),(31,'Courtier','2014-12-06 21:51:46','2014-12-06 21:51:46'),(32,'Environmental Services Marketplace','2014-12-06 21:51:46','2014-12-25 21:01:11'),(33,'Manhattan Residential Group','2014-12-06 21:51:46','2014-12-25 21:00:59'),(34,'Rob Brown','2014-12-06 21:51:46','2014-12-06 21:51:46'),(35,'Josh Kornreich','2014-12-06 21:51:46','2014-12-06 21:51:46'),(36,'Keith Williams','2014-12-06 21:51:46','2014-12-06 21:51:46'),(37,'Tim Nebergall','2014-12-06 21:51:46','2014-12-06 21:51:46'),(38,'Dr. Marcus S. Tappan and Associates','2014-12-06 21:51:46','2014-12-06 21:51:46'),(39,'Networking Advocate','2014-12-06 21:51:46','2014-12-06 21:51:46'),(40,'Quintus McDonald','2014-12-06 21:51:46','2014-12-06 21:51:46'),(41,'The Plaza','2014-12-06 21:51:46','2014-12-06 21:51:46'),(42,'Ali Paskun','2014-12-06 21:51:46','2014-12-06 21:51:46'),(43,'Ryan Douvlos','2014-12-06 21:51:46','2014-12-06 21:51:46'),(44,'Strategic Financial Services','2014-12-06 21:51:46','2014-12-06 21:51:46'),(45,'Barry Kornreich','2014-12-06 21:51:46','2014-12-06 21:51:46'),(46,'United Erie','2014-12-06 21:51:46','2014-12-06 21:51:46'),(47,'Yorktowne Sports','2014-12-06 21:51:46','2014-12-06 21:51:46'),(48,'Nate Mahaffey','2014-12-06 21:51:46','2014-12-06 21:51:46'),(49,'Mia Zachary','2014-12-06 21:59:32','2014-12-06 21:59:32'),(50,'Twin Harbor Web Solutions','2014-12-08 16:58:01','2014-12-08 16:58:01'),(51,'Forester Restaurant & Tavern','2014-12-09 21:21:29','2014-12-09 21:21:29'),(52,'Arrow Tech','2014-12-09 23:14:40','2014-12-09 23:14:40'),(53,'Upfront Ventures','2014-12-11 05:32:33','2014-12-11 05:32:33'),(54,'Adly','2014-12-11 05:33:17','2014-12-11 05:33:17'),(55,'DataSift','2014-12-11 05:33:57','2014-12-11 05:33:57'),(56,'Julie Ringnes','2014-12-25 03:13:19','2014-12-25 20:59:31'),(57,'LazyCoin','2014-12-25 20:43:05','2014-12-25 20:59:23'),(58,'Capital Connections','2014-12-25 21:06:13','2014-12-25 21:06:13'),(59,'Renee Canali','2014-12-25 21:15:46','2014-12-25 21:15:46');
/*!40000 ALTER TABLE `people_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_person`
--

DROP TABLE IF EXISTS `people_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_person`
--

LOCK TABLES `people_person` WRITE;
/*!40000 ALTER TABLE `people_person` DISABLE KEYS */;
INSERT INTO `people_person` VALUES (1,'Magnus','Grimmett','2014-12-06 21:51:46','2014-12-06 21:51:46'),(2,'Glenn','Garnes','2014-12-06 21:51:46','2014-12-06 21:51:46'),(3,'Lisa','LaMagna','2014-12-06 21:51:46','2014-12-06 21:51:46'),(4,'Chirag','Doshi','2014-12-06 21:51:46','2014-12-06 21:51:46'),(5,'Dhaval','Doshi','2014-12-06 21:51:47','2014-12-06 21:51:47'),(6,'Jaimin','Doshi','2014-12-06 21:51:47','2014-12-06 21:51:47'),(7,'Chirayu','Nimish','2014-12-06 21:51:47','2014-12-06 21:51:47'),(8,'Bob','Graham','2014-12-06 21:51:47','2014-12-06 21:51:47'),(9,'Tim','Nebergall','2014-12-06 21:51:47','2014-12-06 21:51:47'),(10,'Bob','Kreamer','2014-12-06 21:51:47','2014-12-06 21:51:47'),(11,'Sommer','Peterson','2014-12-06 21:51:47','2014-12-06 21:51:47'),(12,'Jillian','Horn','2014-12-06 21:51:47','2014-12-06 21:51:47'),(13,'Nicole','Bartol','2014-12-06 21:51:47','2014-12-06 21:51:47'),(14,'Alexander','Cadman','2014-12-06 21:51:47','2014-12-06 21:51:47'),(15,'Christina','Andresen','2014-12-06 21:51:47','2014-12-06 21:51:47'),(16,'Al','Granger','2014-12-06 21:51:47','2014-12-06 21:51:47'),(17,'Ryan','Douvlos','2014-12-06 21:51:47','2014-12-06 21:51:47'),(18,'Jim','Douvlos','2014-12-06 21:51:47','2014-12-06 21:51:47'),(19,'Josh','Kornreich','2014-12-06 21:51:47','2014-12-06 21:51:47'),(20,'Wendy','Sosik','2014-12-06 21:51:47','2014-12-06 21:51:47'),(21,'Stefano','Bertocci','2014-12-06 21:51:47','2014-12-06 21:51:47'),(22,'Deborah','Myers','2014-12-06 21:51:47','2014-12-06 21:51:47'),(23,'Ryan','Nuhfer','2014-12-06 21:51:47','2014-12-06 21:51:47'),(24,'Mark','Butina','2014-12-06 21:51:47','2014-12-06 21:51:47'),(25,'Christy','Calvert-Passinger','2014-12-06 21:51:47','2014-12-06 21:51:47'),(26,'Ron','Mohl','2014-12-06 21:51:47','2014-12-06 21:51:47'),(27,'Mia','Reed','2014-12-06 21:51:47','2014-12-06 21:51:47'),(28,'Lisa','Ridgley','2014-12-06 21:51:47','2014-12-06 21:51:47'),(29,'Paul','Jones','2014-12-06 21:51:47','2014-12-06 21:51:47'),(30,'Alejandro','Uribe','2014-12-06 21:51:47','2014-12-06 21:51:47'),(31,'Yale','Klat','2014-12-06 21:51:48','2014-12-06 21:51:48'),(32,'Rob','Brown','2014-12-06 21:51:48','2014-12-06 21:51:48'),(33,'Keith','Williams','2014-12-06 21:51:48','2014-12-06 21:51:48'),(34,'Marcus','Tappan','2014-12-06 21:51:48','2014-12-06 21:51:48'),(35,'Jeanette','Hordge','2014-12-06 21:51:48','2014-12-06 21:51:48'),(36,'Quintus','McDonald','2014-12-06 21:51:48','2014-12-06 21:51:48'),(37,'Ali','Paskun','2014-12-06 21:51:48','2014-12-06 21:51:48'),(38,'Steve','Hoskinson','2014-12-06 21:51:48','2014-12-06 21:51:48'),(39,'Barry','Kornreich','2014-12-06 21:51:48','2014-12-06 21:51:48'),(40,'Mike','Smith','2014-12-06 21:51:48','2014-12-06 21:51:48'),(41,'Rodney','Blevins','2014-12-06 21:51:48','2014-12-06 21:51:48'),(42,'Jack','Parson','2014-12-06 21:51:48','2014-12-06 21:51:48'),(43,'Nate','Mahaffey','2014-12-06 21:51:48','2014-12-06 21:51:48'),(44,'Mia','Zachary','2014-12-06 21:59:32','2014-12-06 21:59:32'),(45,'Julie','Ringnes','2014-12-08 16:51:00','2014-12-08 16:51:00'),(46,'Michael','McCloy','2014-12-08 16:56:09','2014-12-08 16:56:09'),(47,'Jeff','Gazdak','2014-12-09 21:20:20','2014-12-09 21:20:20'),(48,'Dylan','Moore','2014-12-09 21:25:30','2014-12-09 21:25:30'),(49,'Dillan','Archer','2014-12-09 23:14:28','2014-12-09 23:14:28'),(50,'Brent','Richards','2014-12-09 23:25:48','2014-12-09 23:25:48'),(51,'Mark','Suster','2014-12-11 05:31:42','2014-12-11 05:31:42'),(52,'Danial','Daychopen','2014-12-16 21:30:27','2014-12-16 21:30:27'),(53,'Michael','Douglass','2014-12-25 21:06:18','2014-12-25 21:06:18'),(55,'Renee','Canali','2014-12-25 21:15:46','2014-12-25 21:15:46'),(56,'Melissa','Aymold','2014-12-26 18:48:43','2014-12-26 18:48:43');
/*!40000 ALTER TABLE `people_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_vendor`
--

DROP TABLE IF EXISTS `people_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_vendor_26b2345e` (`organization_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_vendor`
--

LOCK TABLES `people_vendor` WRITE;
/*!40000 ALTER TABLE `people_vendor` DISABLE KEYS */;
INSERT INTO `people_vendor` VALUES (1,15,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(2,16,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(3,17,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(4,18,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(5,19,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(6,20,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(7,21,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(8,22,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(9,23,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(10,24,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(11,25,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(12,26,'2014-12-06 21:51:50','2014-12-06 21:51:50'),(13,27,'2014-12-06 21:51:50','2014-12-06 21:51:50');
/*!40000 ALTER TABLE `people_vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_project`
--

DROP TABLE IF EXISTS `projects_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `code` varchar(5) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `notes` longtext,
  `due` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_project_dc91ed4b` (`status_id`),
  KEY `projects_project_2bfe9d72` (`client_id`),
  KEY `projects_project_8784215c` (`manager_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_project`
--

LOCK TABLES `projects_project` WRITE;
/*!40000 ALTER TABLE `projects_project` DISABLE KEYS */;
INSERT INTO `projects_project` VALUES (1,'Learning Management System','LMS',2,3,NULL,1,NULL,'2015-01-02'),(2,'Aspen Strong Directory','ASD',1,1,3,3,NULL,'2015-01-02'),(3,'Tokome','TOK',2,4,NULL,3,NULL,'2015-01-02'),(4,'Westcon Directory','WCD',3,2,3,3,NULL,'2015-01-02'),(5,'AWARE','',3,4,3,3,NULL,'2015-01-02'),(6,'Reservation System','RES',2,5,3,1,NULL,'2015-01-02'),(7,'Mission Bowling Club Website','MBC',2,5,3,1,NULL,'2015-01-02'),(8,'Sex Love','SL',2,11,3,2,NULL,'2015-01-02'),(9,'LiveAha','',3,10,3,3,NULL,'2015-01-02'),(10,'Forester Website','FOR',2,38,4,2,NULL,'2015-01-02'),(11,'LazyCoin','',3,41,4,3,NULL,'2015-01-02'),(12,'ArrowTech','ART',2,39,4,3,NULL,'2015-01-02'),(13,'Site Maintenance','AGS',2,8,3,2,NULL,'2015-01-02'),(14,'AbundiaNow','ABN',2,40,3,2,'Notes','2015-01-02'),(15,'Capital Connections','',2,42,5,2,NULL,'2015-01-02'),(16,'RFP Trainer','',1,29,3,2,NULL,'2015-01-02'),(17,'Land of Possibility','',1,43,3,3,NULL,'2015-01-02'),(18,'Yorktowne Sports','',1,34,3,2,NULL,'2015-01-02'),(19,'Glenn Garnes','',1,7,3,2,NULL,'2015-01-02'),(20,'Server Migration','',2,9,3,1,NULL,'2015-01-02'),(21,'ConnectDG','',2,37,4,1,NULL,'2015-01-02');
/*!40000 ALTER TABLE `projects_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_projectcomponent`
--

DROP TABLE IF EXISTS `projects_projectcomponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_projectcomponent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `billed` tinyint(1) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_projectcomponent_b098ad43` (`project_id`),
  KEY `projects_projectcomponent_dc91ed4b` (`status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_projectcomponent`
--

LOCK TABLES `projects_projectcomponent` WRITE;
/*!40000 ALTER TABLE `projects_projectcomponent` DISABLE KEYS */;
INSERT INTO `projects_projectcomponent` VALUES (1,'Phase 1',300.00,1,1,5,3),(2,'Phase 2',300.00,1,1,5,3),(3,'Phase 3',300.00,1,1,5,3),(4,'Phase 4',300.00,1,0,5,3),(5,'Phase 1',450.00,1,1,4,3),(6,'Phase 2',750.00,1,0,4,3),(7,'Phase 1',2250.00,1,1,3,3),(8,'Phase 2',2250.00,0,0,3,2),(9,'Phase 3',1500.00,0,0,3,1),(10,'Phase 4',1500.00,0,0,3,1),(11,'Initial form extension',800.00,1,1,2,3),(12,' Category-Keyword pairing',200.00,1,1,2,3),(13,'Issue/Mental Health Diagnosis dropdowns',100.00,1,1,2,3),(14,'Filters',600.00,1,1,2,3),(15,'Conditionally hide credentials',50.00,0,0,2,1),(16,'Change options on landing page',50.00,0,0,2,1),(17,'Limit personal statement',50.00,0,0,2,1),(18,'Prevent users from entering the WordPress admin',50.00,0,0,2,1),(19,'Clear initial search results',50.00,0,0,2,1),(20,'Extend specialty functionality',200.00,0,0,2,1),(21,'Allow front end form editing',300.00,0,0,2,1),(22,'Mandate all fields',50.00,0,0,2,1),(23,'Design single entry page',400.00,0,0,2,1),(24,'Retainer',3442.50,1,1,1,3),(25,'Phase 1',2295.00,1,1,1,3),(26,'Phase 2',2295.00,1,1,1,3),(27,'Phase 3',2295.00,1,1,1,3),(28,'Phase 4',2295.00,0,0,1,2),(29,'Phase 5',2295.00,0,0,1,1),(30,'Phase 6',2295.00,0,0,1,1),(31,'Phase 7',2295.00,0,0,1,1),(32,'Phase 8',3442.50,0,0,1,1),(33,'Initial development',1350.00,1,1,6,3),(34,'Refactor into Laravel',1500.00,1,1,6,3),(35,'Bug fixes',350.00,0,0,6,2),(36,'Initial Design',500.00,1,1,7,3),(37,'Email configuration',250.00,0,0,7,2),(38,'Retainer',1600.00,1,1,8,3),(39,'Continuation retainer',800.00,1,1,8,2),(40,'Final payment',800.00,1,0,8,1),(41,'Product page design fixes',250.00,1,1,9,3),(42,'Retainer',900.00,1,1,10,3),(43,'Development',900.00,0,0,10,2),(44,'Project',2400.00,1,1,11,3),(45,'Phase 1',720.00,1,1,12,2),(46,'Phase 2',540.00,1,1,12,3),(47,'Phase 3',540.00,1,1,12,3),(48,'Merging of REIA Marketing',100.00,0,0,13,3),(49,'Merging of Events Calendar Info',400.00,0,0,13,2),(50,'WordPress Updates',150.00,0,0,13,1),(51,'Retainer',600.00,1,1,14,2),(52,'Development',600.00,0,0,14,1),(53,'Website Migration',49.99,0,0,15,2),(54,'Confirm migration of website and provide email credentials',20.00,0,0,16,1),(55,'Set up hosting account and WordPress installation for GovConOnline',49.99,0,0,16,1),(56,'Purchase and install SSL certificate for RFP Trainer',50.00,0,0,16,1),(57,'Clean up phishing attack notifications',50.00,0,0,17,1),(58,'Remove tax from out of state orders',50.00,0,0,18,1),(59,'Deactivate old store categories',50.00,0,0,18,1),(60,'URL connection issue on Bold New Economy',75.00,0,0,19,1),(61,'Complete migration',1000.00,0,0,20,2),(62,'ConnectDG',500.00,0,0,21,2);
/*!40000 ALTER TABLE `projects_projectcomponent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_projectstatus`
--

DROP TABLE IF EXISTS `projects_projectstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_projectstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_projectstatus`
--

LOCK TABLES `projects_projectstatus` WRITE;
/*!40000 ALTER TABLE `projects_projectstatus` DISABLE KEYS */;
INSERT INTO `projects_projectstatus` VALUES (1,'Pending'),(2,'Active'),(3,'Complete');
/*!40000 ALTER TABLE `projects_projectstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_task`
--

DROP TABLE IF EXISTS `projects_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `description` longtext,
  `person_id` int(11) DEFAULT NULL,
  `project_id` int(11),
  `hours_worked` int(11) NOT NULL,
  `minutes_worked` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `complete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_task_a8452ca7` (`person_id`),
  KEY `projects_task_b098ad43` (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_task`
--

LOCK TABLES `projects_task` WRITE;
/*!40000 ALTER TABLE `projects_task` DISABLE KEYS */;
INSERT INTO `projects_task` VALUES (1,'2015-01-05','Setup the WordPress Installation',3,14,0,0,2,0),(2,'2015-01-05','Install the plugins',3,14,0,0,2,0),(3,'2015-01-05','Point the domain to our server\r\n',3,14,0,0,2,0),(4,'2015-01-12','Program project link on Tasks view to link to the project, not the task',3,NULL,0,0,2,0);
/*!40000 ALTER TABLE `projects_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_worklog`
--

DROP TABLE IF EXISTS `projects_worklog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_worklog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `notes` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_worklog`
--

LOCK TABLES `projects_worklog` WRITE;
/*!40000 ALTER TABLE `projects_worklog` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_worklog` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-05 13:29:28
