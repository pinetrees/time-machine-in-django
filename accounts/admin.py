from django.contrib import admin
from accounts.models import Subscription, HostingAccount, InternalAccount, InternalTransaction, Draw, TransactionRelation

class HostingAccountAdmin(admin.ModelAdmin):
	list_display = ["name", "domain", "activity_ranking", "email_accounts", "ip", "current_server", "site_down", "site_compromised", "marked_for_deletion"]
	list_editable = ["email_accounts", "ip", "site_down", "site_compromised", "marked_for_deletion"]
	list_filter = ["ip", "email_accounts"]

class InternalAccountAdmin(admin.ModelAdmin):
	list_display = ["name", "balance"]

class TransactionRelationInline(admin.TabularInline):
	model = TransactionRelation
	fk_name = 'primary_transaction'

class InternalTransactionAdmin(admin.ModelAdmin):
	list_display = ["from_account", "to_account", "amount", "project", "vendor"]
	list_filter = ["from_account", "to_account"]
	inlines = [TransactionRelationInline]

# Register your models here.
admin.site.register(Subscription)
admin.site.register(HostingAccount, HostingAccountAdmin)
admin.site.register(InternalAccount, InternalAccountAdmin)
admin.site.register(InternalTransaction, InternalTransactionAdmin)
admin.site.register(Draw)
