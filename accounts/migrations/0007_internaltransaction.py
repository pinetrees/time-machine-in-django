# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_internalaccount_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='InternalTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('from_account', models.ForeignKey(related_name='from_account', to='accounts.InternalAccount')),
                ('to_account', models.ForeignKey(related_name='to_account', to='accounts.InternalAccount')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
