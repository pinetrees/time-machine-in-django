# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0015_auto_20150103_2140'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('percentage', models.DecimalField(default=1.0, max_digits=3, decimal_places=2)),
                ('amount', models.DecimalField(default=0.0, max_digits=7, decimal_places=2)),
                ('primary_transaction', models.ForeignKey(related_name='primary', to='accounts.InternalTransaction')),
                ('related_transaction', models.ForeignKey(related_name='related', to='accounts.InternalTransaction')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='transactionrelations',
            name='primary_transaction',
        ),
        migrations.RemoveField(
            model_name='transactionrelations',
            name='related_transaction',
        ),
        migrations.DeleteModel(
            name='TransactionRelations',
        ),
    ]
