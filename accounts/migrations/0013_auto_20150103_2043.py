# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0012_internaltransaction_project'),
    ]

    operations = [
        migrations.AlterField(
            model_name='internaltransaction',
            name='project',
            field=models.ForeignKey(default=None, blank=True, to='projects.Project', null=True),
            preserve_default=True,
        ),
    ]
