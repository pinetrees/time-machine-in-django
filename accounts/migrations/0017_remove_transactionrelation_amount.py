# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0016_auto_20150103_2141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transactionrelation',
            name='amount',
        ),
    ]
