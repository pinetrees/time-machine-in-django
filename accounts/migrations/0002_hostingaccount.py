# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0010_auto_20141221_0130'),
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HostingAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('domain', models.CharField(max_length=128)),
                ('person', models.ForeignKey(to='people.Person')),
                ('client', models.ForeignKey(to='people.Client')),
                ('subscription', models.ForeignKey(to='accounts.Subscription')),
                ('approximate_mb', models.IntegerField()),
                ('activity_ranking', models.DecimalField(max_digits=3, decimal_places=2)),
                ('ip', models.CharField(max_length=32)),
                ('email_accounts', models.IntegerField()),
                ('current_server', models.CharField(max_length=64)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
