# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0022_auto_20150103_2034'),
        ('accounts', '0011_remove_internaltransaction_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='internaltransaction',
            name='project',
            field=models.ForeignKey(default=1, to='projects.Project'),
            preserve_default=False,
        ),
    ]
