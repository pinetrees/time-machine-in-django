# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0008_auto_20150103_2034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='internaltransaction',
            name='project',
            field=models.ForeignKey(to='projects.Project'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='internaltransaction',
            name='vendor',
            field=models.ForeignKey(to='people.Vendor'),
            preserve_default=True,
        ),
    ]
