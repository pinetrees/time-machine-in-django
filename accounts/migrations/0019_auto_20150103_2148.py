# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0018_transactionrelation_amount'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transactionrelation',
            old_name='percentage',
            new_name='ratio',
        ),
    ]
