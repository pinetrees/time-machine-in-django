# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0021_auto_20150103_2008'),
        ('projects', '0022_auto_20150103_2034'),
        ('accounts', '0007_internaltransaction'),
    ]

    operations = [
        migrations.AddField(
            model_name='internaltransaction',
            name='amount',
            field=models.DecimalField(default=0.0, max_digits=7, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='internaltransaction',
            name='project',
            field=models.ForeignKey(default=None, blank=True, to='projects.Project', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='internaltransaction',
            name='vendor',
            field=models.ForeignKey(default=None, blank=True, to='people.Vendor', null=True),
            preserve_default=True,
        ),
    ]
