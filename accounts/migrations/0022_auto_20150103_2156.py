# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0021_auto_20150103_2156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactionrelation',
            name='ratio',
            field=models.DecimalField(default=1.0, max_digits=5, decimal_places=4),
            preserve_default=True,
        ),
    ]
