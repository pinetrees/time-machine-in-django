# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_hostingaccount'),
    ]

    operations = [
        migrations.AddField(
            model_name='hostingaccount',
            name='site_compromised',
            field=models.BooleanField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hostingaccount',
            name='site_down',
            field=models.BooleanField(default=0),
            preserve_default=True,
        ),
    ]
