from core import models
from people.models import Person, Client, Vendor
from projects.models import Project
from timemachine import _math_

# Create your models here.
class Subscription(models.Model):
	client = models.ForeignKey(Client)
	revenue = models.DecimalField(max_digits=5, decimal_places=2)
	periods_per_year = models.IntegerField()

	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.client.__str__()

class HostingAccount(models.Model):
	name = models.CharField(max_length=32)
	domain = models.CharField(max_length=128)
	#person = models.ForeignKey(Person)
	#client = models.ForeignKey(Client)
	subscription = models.ForeignKey(Subscription)
	approximate_mb = models.IntegerField()
	activity_ranking = models.DecimalField(max_digits=3, decimal_places=2)
	ip = models.CharField(max_length=32)
	email_accounts = models.IntegerField()
	current_server = models.CharField(max_length=64)
	site_down = models.BooleanField(default=False)
	site_compromised = models.BooleanField(default=False)
	marked_for_deletion = models.BooleanField(default=False)

	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class InternalAccount(models.TimeMachineModel):
	name = models.CharField(max_length=200)

	def incoming(self):
		return InternalTransaction.objects.filter(to_account=self)

	def outgoing(self):
		return InternalTransaction.objects.filter(from_account=self)

	@classmethod
	def clean_aggregation(self, dictionary, key):
		value = dictionary[key]
		if value is not None:
			return value
		else:
			return 0

	def increase(self):
		amount__sum__dict = self.incoming().aggregate(models.Sum('amount'))		
		return self.clean_aggregation(amount__sum__dict, 'amount__sum')

	def decrease(self):
		amount__sum__dict = self.outgoing().aggregate(models.Sum('amount'))		
		return self.clean_aggregation(amount__sum__dict, 'amount__sum')

	def balance(self):
		return self.increase() - self.decrease()

class InternalTransaction(models.Model):
	from_account = models.ForeignKey(InternalAccount, related_name="from_account")
	to_account = models.ForeignKey(InternalAccount, related_name="to_account")
	amount = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)
	project = models.ForeignKey(Project, null=True, blank=True, default=None)
	vendor = models.ForeignKey(Vendor, null=True, blank=True, default=None)
	notes = models.TextField(null=True, blank=True)

	def __str__(self):
		if self.project is not None:
			primary_identifier = self.project
		elif self.vendor is not None:
			primary_identifier = self.vendor
		else:
			primary_identifier = self.to_account
		return " :: ".join([primary_identifier.__str__(), self.amount.__str__()])

	def related_transactions_amount(self):
		return _math_.clean_aggregation(self.primary.aggregate(models.Sum('amount')), 'amount__sum')

	def related_transactions_total(self):
		#This is garbage
		return _math_.clean_aggregation(self.primary.aggregate(models.Sum('related_transaction__amount')), 'related_transaction__amount__sum')

	def save(self, *args, **kwargs):
		if self.amount == 0:
			self.amount = self.related_transactions_amount()
		super(InternalTransaction, self).save(*args, **kwargs)

class Draw(models.DateModel):
	pass

class TransactionRelation(models.Model):
	primary_transaction = models.ForeignKey(InternalTransaction, related_name="primary")
	related_transaction = models.ForeignKey(InternalTransaction, related_name="related")
	ratio = models.DecimalField(max_digits=5, decimal_places=4, default=1.00)
	amount = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)

	def save(self, *args, **kwargs):
		if self.amount == 0:
			self.amount = self.ratio * self.related_transaction.amount
		if self.ratio == 0:
			self.ratio = self.amount / self.related_transaction.amount
		super(TransactionRelation, self).save(*args, **kwargs)
